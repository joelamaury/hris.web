﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace HRIS.Helpers
{
    public class DateUtils
    {
        public static int GetWeekOfMonth(DateTime date, CultureInfo cultureInfo)
        {
            int weekYear = cultureInfo.Calendar.GetWeekOfYear(date, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
            int firstDayMonthWeek = cultureInfo.Calendar.GetWeekOfYear(new DateTime(date.Year, date.Month, 1), System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);

            return (weekYear - firstDayMonthWeek) + 1;
        }

        public static int GetWeekNumber(DateTime date, CultureInfo cultureInfo)
        {
            return cultureInfo.Calendar.GetWeekOfYear(date, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }

        internal static DateTime NumberToDateTime(int? number)
        {
            var defaultTime = new DateTime(new TimeSpan(0, 0, 0).Ticks);
            return number.HasValue ? new DateTime(new TimeSpan(0, Math.Abs(number.Value), 0).Ticks) : defaultTime;
        }

        internal static Int32 DateTimeToNumber(DateTime dateTime)
        {
            return Convert.ToInt32(
               new TimeSpan
                   (
                       Convert.ToInt32(dateTime.Hour),
                       Convert.ToInt32(dateTime.Minute), 
                       0)
                       .TotalMinutes
                   );
        }

        public static int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}