﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ITSV6.Model
{
    public class StringUtils
    {
        public static string StatusToString(bool status)
        {
            return (status) ? "A" : "N";
        }

        public static Boolean YesNoToBoolean(string boolean)
        {
            return ( !String.IsNullOrEmpty(boolean) && boolean.Equals("Y")) ? true : false;
        }

        public static String BooleanToYesNo(Boolean boolean)
        {
            return (boolean) ? "Y" : "N";
        }

        public static Boolean ActiveInactiveToBoolean(string boolean)
        {
            return (boolean.Equals("A")) ? true : false;
        }

        public static String BooleanToActiveInactive(Boolean boolean)
        {
            return (boolean) ? "A" : "I";
        }

        public static T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return default(T); // returns the default value for the type
            }
            else
            {
                return (T)obj;
            }
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            const string validUpperChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string validNumber = "1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            int index = 0;
            while (0 < length--)
            {
                if (index == 0)
                    res.Append(validUpperChar[rnd.Next(validUpperChar.Length)]);

                if (index == 1)
                    res.Append(validNumber[rnd.Next(validNumber.Length)]);

                if (index > 1)
                    res.Append(valid[rnd.Next(valid.Length)]);

                index++;
            }
            return res.ToString();
        }
        public static String Unencrypt(String publicText)
        {
            String publicTextTmp = publicText;
            String secretText = String.Empty;
            Int32 index = 0;
            Int32 chr = 0;

            try
            {
                foreach (var part in publicTextTmp.Split('a'))
                {
                    if (!String.IsNullOrEmpty(part))
                    {
                        chr = Convert.ToInt32(part.Split('i')[0]);
                        index = Convert.ToInt32(part.Split('i')[1]);
                        secretText += (char)(chr - index);
                    }
                }
            }
            catch (Exception)
            {
                secretText = publicText;
            }

            return secretText;
        }

        /// <summary>
        /// Encriptar una cadena recibida como parametro [secretText] devolviendo su correspondiente cadena encriptada
        /// Para ello se toma caracter por caracter y se lo convierte a su valor ASCII mÃ¡s un Ã­ndice obtenido en forma random. 
        /// De esta manera una misma clave puede ser convertida en forma diferente cada vez que se encripte.
        /// </summary>
        public static String EncryptText(String secretText)
        {
            String publicText = String.Empty;
            Random numberRandom = new Random();
            Int32 index = 0;
            Int32 chr = 0;

            for (int i = 0; i < secretText.Length; i++)
            {
                index = numberRandom.Next(0, 99);
                chr = Encoding.Unicode.GetBytes(secretText.Substring(i, 1)).FirstOrDefault() + index;
                publicText += String.Format("a{0}i{1}", chr, index);
            }

            return String.IsNullOrEmpty(publicText) ? String.Empty : String.Format("{0}a", publicText);
        }

        public static String MaskSSN(String ssn)
        {
            if (ssn.Length > 0)
            {
                if (ssn.Contains('-'))
                {
                    string _ssnAux = string.Empty;
                    int count = 0;
                    char[] arrSSN = ssn.ToCharArray();
                    for (int i = 0; i < arrSSN.Length; i++)
                    {
                        if (count == 2)
                        {
                            break;
                        }
                        else
                        {
                            if (arrSSN[i] == '-')
                                count++;
                            else
                                arrSSN[i] = 'X';

                        }
                    }
                    for (int i = 0; i < arrSSN.Length; i++)
                        _ssnAux += arrSSN[i];
                    return _ssnAux;
                }
                else
                {
                    return ssn.Substring(ssn.Length - 4, 4).PadLeft(ssn.Length, 'X');
                }
            }
            else
            {
                return ssn;
            }
        }       
    }
}