﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITSV6.Helpers
{
    public class TimeUtils
    {
        public static string convertMinutestoHH24(int minutes)
        {
            if (minutes != 0)
            {
                return new DateTime(new TimeSpan(0, minutes, 0).Ticks).ToString("HH:mm");
            }
            return "00:00";
        }

        public static int convertHH24toMinutes(string hours)
        {
            if (hours != "00:00")
            {
                return Convert.ToInt32(new TimeSpan(Convert.ToInt32(hours.Split(':')[0]),
                                                    Convert.ToInt32(hours.Split(':')[1]), 0).TotalMinutes);
            }
            return 0;
        }

        public static string convertHHtoMinutes(long ticks)
        {
            if (ticks != 0)
            {
                TimeSpan t = new TimeSpan(ticks);
                return string.Format("{0}:{1}", t.TotalHours.ToString("00"), t.Minutes.ToString("00"));
            }
            return "00:00";
        }
    
		public static Decimal AmountInTimeFromHours(String hoursQty)
        {
            Decimal secondsQty = 0;

            if (!String.IsNullOrEmpty(hoursQty))
                if (hoursQty.Split(':').Count() > 1)
                    secondsQty = Math.Round(((Convert.ToDecimal(hoursQty.Split(':')[0]) + (Convert.ToDecimal(hoursQty.Split(':')[1]) / 60) * (hoursQty.Contains("-") ? -1 : 1)) * 3600));
                else
                    secondsQty = Convert.ToInt32(hoursQty) * 3600;
            else
                secondsQty = 0;

            return secondsQty;
        }
        
        public static Decimal AmountInTimeFrom12HoursFormat(String timeVal)
        {
            // Create vars
            Boolean IsPM = timeVal.Contains("PM");
            Int32 hours = Convert.ToInt32(timeVal.Split(':')[0]);

            // Remove PM/AM
            timeVal = timeVal.Remove(timeVal.IndexOfAny(new char[] { 'P', 'A' })).Trim();

            // If is PM and not 12 PM, add 12
            if (IsPM)
            { if (hours < 12) { hours += 12; } }
            // If is 12 AM, reset to zero
            else
            { if (hours == 12) { hours = 0; } }

            // Replace hours in time if changed
            if (hours != Convert.ToInt32(timeVal.Split(':')[0]))
            { timeVal = timeVal.Remove(0, timeVal.IndexOf(':')).Replace(":", hours.ToString() + ":"); }

            // Convert hours in 24hr format
            return Convert.ToInt32(AmountInTimeFromHours(timeVal));
        }

        public static String AmountInTimeFromSeconds(int secondsQty)
        {
            return String.Format("{0}:{1:00}", (TimeSpan.FromSeconds(secondsQty).Hours + (TimeSpan.FromSeconds(secondsQty).Days * 24)), TimeSpan.FromSeconds(secondsQty < 0 ? (secondsQty * -1) : secondsQty).Minutes);
        }

        public static String AmountInTime12FromSeconds(int secondsQty)
        {
            String[] arr = AmountInTimeFromSeconds(secondsQty).Split(':');

            String AmPm = String.Empty;

            AmPm = (Convert.ToInt32(arr[0]) >= 12) ? "PM" : "AM";

            int hours = (Convert.ToInt32(arr[0]) > 12) ? Convert.ToInt32(arr[0]) - 12 : Convert.ToInt32(arr[0]);

            hours = (hours == 0) ? 12 : hours;

            return String.Format("{0}:{1} {2}", hours, arr[1], AmPm);
        }

        public static String AmountInTime24FromSeconds(int secondsQty)
        {
            String[] arr = AmountInTimeFromSeconds(secondsQty).Split(':');

            int hours = Convert.ToInt32(arr[0]);

            return String.Format("{0}:{1}", hours, arr[1]);
        }

        public static string TimeSpanToHHmmFormat(TimeSpan? timeSpanVal)
        {
            decimal decPart = 0;
            int intPart = 0;

            if (timeSpanVal.HasValue)
            {
                intPart = (int)timeSpanVal.Value.TotalHours;
                decPart = intPart - ((decimal)timeSpanVal.Value.TotalHours);
            }

            return intPart.ToString() + TimeSpan.FromHours((double)decPart).ToString("\\:mm");
        }

        public static string StringDecimalToHHmmFormat(string value)
        {
            decimal val = 0;

            decimal.TryParse(value, out val);

            return DecimalToHHmmFormat(val);           
        }

        public static string DecimalToHHmmFormat(decimal value)
        {
            decimal decPart = 0;
            int intPart = 0;

            intPart = (int)value;
            decPart = (intPart - ((decimal)value)) * -1;

            return intPart.ToString() + ":" + Math.Round(decPart * 60, 0).ToString();
        }
    }
}