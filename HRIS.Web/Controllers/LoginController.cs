﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Resources;
using NLog;
using System.Threading;
using System.Globalization;
using HRIS.Helpers;
using ITSV6.Model;
using HRIS.Managers;
using API_Provider.Core;
using Models;

namespace HRIS.Controllers
{
    public class LoginController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private void saveCookies(String user, String password, Boolean rememberMe)
        {
            HttpCookie cookie = Request.Cookies["user"];
            if (cookie == null)
            {
                cookie = new HttpCookie("user", user);
                Response.Cookies.Add(cookie);
            }
            if (rememberMe)
            {
                cookie.Expires = DateTime.Now.AddMonths(12);
                cookie.Value = user;
                Response.Cookies.Set(cookie);
            }
            else
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Set(cookie);
            }
        }

        [HttpGet]
        public ActionResult Index(String initialMessage)
        {
            HttpCookie lang = Request.Cookies["HRISLanguage"];
            if (lang != null)
            {
                int language = 0;
                int.TryParse(lang.Value, out language);

                if (language == 0)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                }
                else if (language == 1)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-US");
                }
                else
                {
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                }

                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
                Session["CurrentCulture"] = language;
            }

            if (Session["user"] == null)
            {
                HttpCookie cookie = Request.Cookies["user"];
                if (cookie != null)
                {
                    ViewData["storedUser"] = cookie.Values["id"];
                    String password = (String)cookie.Values["psw"];
                    if (!String.IsNullOrEmpty(password))
                    {
                        password = Encryptor.Unencrypt(password);
                        ViewData["storedPass"] = password;
                    }
                }
                if (!String.IsNullOrEmpty(initialMessage))
                    ViewData["initialMessage"] = Resource.YouAreSafelyLoggedOutPleaseComeBackSoon;

            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult Index(String user, String password, bool rememberme = false)
        {
            try
            {
                ViewData["Error"] = String.Empty;
                //   User authenticatedUser = null;
                try
                {
                    bool employeeSearch, processedFiles, screenFields, profilepermissions, fullAccess;
                    var context = GetKronosContext();
                    KronosProvider provider = new KronosProvider(user, password);
                    USERACCOUNT userAcc = context.USERACCOUNTs.Where(u => u.USERACCOUNTNM == user).FirstOrDefault();
                    List<ProfilePermission> permissionList = GetHrisContext().ProfilePermissions.Where(p => p.LogonProfileId == userAcc.LOGONPROFILEID).ToList();
                    Session.Add("user", userAcc);
                    SetPermissionOnSession(userAcc, permissionList, out employeeSearch, out processedFiles, out screenFields, out profilepermissions, out fullAccess);

                    string action, controller;

                    if(processedFiles)
                    {
                        action = "Index";
                        controller = "ProcessedFiles";
                    }
                    else
                    {
                        if (employeeSearch)
                        {
                            action = "Index";
                            controller = "EmployeeSearch";
                        }
                        else
                        {
                            if (screenFields)
                            {
                                action = "ScreenFields";
                                controller = "Configuration";
                            }
                            else
                            {
                                if (profilepermissions)
                                {
                                    action = "ProfilePermissions";
                                    controller = "Configuration";
                                }
                                else
                                {
                                    action = "Index";
                                    controller = "ProcessedFiles";
                                }
                            }
                        }
                    }

                    return RedirectToAction(action, controller);
                }
                catch (Exception ex)
                {
                    ViewData["Error"] = Resource.WrongUserNameOrPassword;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return View();

        }

        private void SetPermissionOnSession(USERACCOUNT userAcc, List<ProfilePermission> permissionList, out bool employeeSearch, out bool processedFiles, out bool screenFields, out bool profilepermissions, out bool fullAccess)
        {
            Session["ProcessedFilesPermission"] = processedFiles = permissionList.Where(p => p.AppSection.Name == "ProcessedFiles" || p.AppSection.Name == "ApplicationFullAccess").Count() > 0 || userAcc.USERACCOUNTNM.ToLower() == "superuser" ? true : false;
            Session["SearchEmployeePermission"] = employeeSearch = permissionList.Where(p => p.AppSection.Name == "SearchEmployee" || p.AppSection.Name == "ApplicationFullAccess").Count() > 0 || userAcc.USERACCOUNTNM.ToLower() == "superuser" ? true : false;
            Session["ScreenFieldsPermission"] = screenFields = permissionList.Where(p => p.AppSection.Name == "ScreenFields" || p.AppSection.Name == "ApplicationFullAccess").Count() > 0 || userAcc.USERACCOUNTNM.ToLower() == "superuser" ? true : false;
            Session["ProfilePermissions"] = profilepermissions = permissionList.Where(p => p.AppSection.Name == "ProfilePermissions" || p.AppSection.Name == "ApplicationFullAccess").Count() > 0 || userAcc.USERACCOUNTNM.ToLower() == "superuser" ? true : false;
            Session["ApplicationFullAccessPermission"] = fullAccess = permissionList.Where(p => p.AppSection.Name == "ApplicationFullAccess").Count() > 0 || userAcc.USERACCOUNTNM.ToLower() == "superuser" ? true : false;
        }       

        public ActionResult ForgotPassword()
        {
            ViewData["Error"] = string.Empty;
            return View();
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "Login");
        }

        public ActionResult ChangeCurrentCulture(int id)
        {

            // Set Cookie value if exists, if not exists create one
            if (Request.Cookies["HRISLanguage"] == null)
            {
                HttpCookie langCookie = new HttpCookie("HRISLanguage");
                langCookie.Value = id.ToString();
                langCookie.Expires = DateTime.Today.AddYears(10);
                Response.Cookies.Add(langCookie);
            }
            else
            {
                HttpCookie langCookie = Request.Cookies["HRISLanguage"];
                langCookie.Value = id.ToString();
                langCookie.Expires = DateTime.Today.AddYears(10);
                Response.Cookies.Set(langCookie);
            }

            //  
            // Change the current culture for this user.  
            //  
            CultureHelper.CurrentCulture = id;
            //  
            // Cache the new current culture into the user HTTP session.   
            //  
            Session["CurrentCulture"] = id;
            //  
            // Redirect to the same page from where the request was made!   
            //  
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}