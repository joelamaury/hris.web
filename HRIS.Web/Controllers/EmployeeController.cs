﻿using HRIS.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace HRIS.Controllers
{

    public class EmployeeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            int personId = (int)TempData["PersonIdNo"];
            DateTime effectiveDate = (DateTime)TempData["EffectiveDate"];
            ViewData["PersonId"] = personId;
            ViewData["EffectiveDate"] = effectiveDate;
            KronosEntities context = GetKronosContext();

            ///Employee Header Info
            string empFullName = string.Empty;
            tPERSON person = context.tPERSONS.Where(p => p.PersonIdNo == personId && p.PersonFromEffectDate <= effectiveDate && p.PersonToEffectDate >= effectiveDate).FirstOrDefault();
            if (person != null)
            {
                empFullName = person.LastName.Trim() + ", " + person.FirstName.Trim();
                if (!string.IsNullOrEmpty(person.MiddleName))
                {
                    empFullName += " " + person.MiddleName;
                }

                tEMPLOYMENT_STATUS status = context.tEMPLOYMENT_STATUS.Where(p => p.PersonIdNo == personId && p.EmploymentStatusFromEffectDate <= effectiveDate && p.EmploymentStatusToEffectDate >= effectiveDate).FirstOrDefault();
                if (status != null && !string.IsNullOrEmpty(status.EmpNo))
                {
                    empFullName += " (" + status.EmpNo + ")";
                }
            }
            ViewData["EmpFullName"] = empFullName;

            ///GetGridData
            ViewData["AddressList"] = GetPersonAddresses(personId, context);
            ViewData["PhoneList"] = GetPersonPhones(personId, context);
            ViewData["EmailList"] = GetPersonEmails(personId, context);
            ViewData["PositionList"] = GetPersonPositions(personId, effectiveDate, context);


            ///PayStatus Data
            List<tPERSON_POSITIONS> ppList = context.tPERSON_POSITIONS.Where(p => p.PersonIdNo == personId).ToList();
            List<HRISListObject> posListObj = new List<HRISListObject>();
            List<HRISListObject> primaryPosListObj = new List<HRISListObject>();
            List<HRISListObject> secondaryPosListObj = new List<HRISListObject>();

            primaryPosListObj.AddRange(ppList.Where(p => p.PositionPrimaryInd && p.PositionFromEffectDate <= effectiveDate && p.PositionToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.tPOSITION_IDS != null && p.tPOSITION_IDS.tPOSITION_CODES != null && p.tPOSITION_IDS.tPOSITION_CODES.FirstOrDefault() != null ? p.tPOSITION_IDS.tPOSITION_CODES.FirstOrDefault().PositionCodeDescription : string.Empty, Value = p.PositionIdNo.ToString() }).Distinct().ToList());
            secondaryPosListObj.AddRange(ppList.Where(p => p.PositionPrimaryInd == false && p.PositionFromEffectDate <= effectiveDate && p.PositionToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.tPOSITION_IDS != null && p.tPOSITION_IDS.tPOSITION_CODES != null && p.tPOSITION_IDS.tPOSITION_CODES.FirstOrDefault() != null ? p.tPOSITION_IDS.tPOSITION_CODES.FirstOrDefault().PositionCodeDescription : string.Empty, Value = p.PositionIdNo.ToString() }).Distinct().ToList());

            string personPositionSelectedVal = string.Empty;

            if (primaryPosListObj.Count > 0)
            {
                personPositionSelectedVal = primaryPosListObj[0].Value.ToString();
            }
            else
            {
                if (secondaryPosListObj.Count > 0)
                {
                    personPositionSelectedVal = secondaryPosListObj[0].Value.ToString();
                }
            }

            int pp;
            if (!string.IsNullOrEmpty(personPositionSelectedVal) && int.TryParse(personPositionSelectedVal, out pp))
            {
                ViewData["PayStatusList"] = GetPayStatus(personId, effectiveDate, pp, context);
            }

            ViewData["PayStatusPosition"] = personPositionSelectedVal;
            posListObj.AddRange(primaryPosListObj);
            posListObj.AddRange(secondaryPosListObj);
            ViewData["PayStatusPositionList"] = posListObj.Select(x => new HRISListObject { Description = x.Description, Value = x.Value }).Distinct().ToList(); ;

            TempData["PersonIdNo"] = personId;
            TempData["EffectiveDate"] = effectiveDate;

            return View();
        }

        #region Personal Info

        public ActionResult _PersonalInfo(int personId, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();
            tPERSON personInfo = context.tPERSONS.Where(p => p.PersonIdNo == personId && p.PersonFromEffectDate <= effectiveDate && p.PersonToEffectDate >= effectiveDate).FirstOrDefault();

            if (personInfo == null)
            {
                personInfo = new tPERSON();
            }

            ViewData["MaritalStatusList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "MS" || p.PersonCodeType == "NA").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();
            ViewData["GenderList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "GN" || p.PersonCodeType == "NA").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();
            ViewData["EthnicOriginList"] = context.tETHNIC_CODES.Where(p => p.EthnicCodeFromEffectDate <= effectiveDate && p.EthnicCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.EthnicCode, Value = p.EthnicCodeIdNo.ToString() }).ToList();
            ViewData["DisabledTypeList"] = context.tMEDICAL_CODES.Where(p => p.MedicalCodeType == "PI" || p.MedicalCodeType == "NA" && p.MedicalCodeFromEffectDate <= effectiveDate && p.MedicalCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.MedicalCode, Value = p.MedicalCodeIdNo.ToString() }).ToList();//vALIDATE
            ViewData["BloodTypeList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "BT" || p.PersonCodeType == "NA").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();

            ViewData["BloodFactorList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "BF" || p.PersonCodeType == "NA").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();

            ViewData["CitizenshipList"] = context.tTYPE_CODES.Where(p => p.TypeCodeType == "CZ" || p.TypeCodeType == "NA" && p.TypeCodeFromEffectDate <= effectiveDate && p.TypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.TypeCode, Value = p.TypeCodeIdNo.ToString() }).ToList(); //vALIDATE
            ViewData["MilitaryServiceList"] = context.tTYPE_CODES.Where(p => p.TypeCodeType == "MS" || p.TypeCodeType == "NA" && p.TypeCodeFromEffectDate <= effectiveDate && p.TypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.TypeCode, Value = p.TypeCodeIdNo.ToString() }).ToList();
            ViewData["VisaTypeList"] = context.tVISA_TYPE_CODES.Where(p => p.VisaTypeCodeFromEffectDate <= effectiveDate && p.VisaTypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.VisaType, Value = p.VisaType.ToString() }).ToList();

            string userName = string.Empty;
            tSELF_ACCESS access = context.tSELF_ACCESS.Where(s => s.PersonIdNo == personId).FirstOrDefault();
            if (access != null)
            {
                userName = access.UserName;
            }

            ViewData["UserName"] = userName;

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Personal Info").FirstOrDefault();

            if (module != null)
            {
                ViewData["PersonalInfoDisabledFields"] = GetDisabledFields(module);
            }

            return View(personInfo);
        }


        public ActionResult SavePersonalInfo(tPERSON person, string neworupdate, DateTime? effectiveDate, string newUserName, string userName)
        {
            person = SaveKronosPersonalInfo(person, neworupdate, effectiveDate, newUserName, userName);
            if (person != null)
            {
                return Json(new
                {
                    status = "success",
                    personIdNo = person.PersonIdNo,
                    personSeqNo = person.PersonSeqNo,
                    timestamp = Convert.ToBase64String(person.Timestamp),
                    effectivefrom = person.PersonFromEffectDate.ToShortDateString(),
                    effectiveto = person.PersonToEffectDate.ToShortDateString()
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tPERSON SaveKronosPersonalInfo(tPERSON person, string neworupdate, DateTime? effectiveDate, string newUserName, string userName)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                System.Data.Entity.Core.Objects.ObjectParameter idNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonIdNo", typeof(Int32));
                idNo.Value = person.PersonIdNo;

                System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonSeqNo", typeof(Int32));
                seqNo.Value = person.PersonSeqNo;

                double? personUserDefined4 = null;

                if (person.PersonUserDefined4.HasValue)
                    personUserDefined4 = (double)person.PersonUserDefined4.Value;

                context.sp_savePersonCtrl(
                    idNo,
                   seqNo,
                   person.BirthDate,
                   person.DisabledInd,
                   person.DisabilityTypeIdNo,
                   person.DisabledVetInd,
                   person.FirstName,
                   person.LastName,
                   person.NameSuffix,
                   person.MaritalStatusIdNo,
                   person.MiddleName,
                   person.MilitaryServiceIdNo,
                   person.MilitarySeparationDate,
                   person.Nickname,
                   person.EthnicCodeIdNo,
                   person.Salutation,
                   person.GenderIdNo,
                   person.SmokerInd,
                   person.PersonTaxIdNo,
                   person.VietnamVetInd,
                   person.OtherVetInd,
                   person.BloodTypeCodeIdNo,
                   person.BloodRHFactorIdNo,
                   person.Height,
                   person.Weight,
                   person.ForeignNationalRegistrationNo,
                   person.CitizenshipIdNo,
                   person.VisaExpirationDate,
                   person.VisaType,
                   person.PersonUserDefined1,
                   person.PersonUserDefined2,
                   person.PersonUserDefined3,
                   personUserDefined4,
                   neworupdate == "n" ? effectiveDate : person.PersonFromEffectDate,
                   person.PersonToEffectDate,
                   person.Timestamp,
                   newUserName != userName ? newUserName : null,
                   person.PersonUserDefined5,
                   person.PersonUserDefined6,
                   person.ArmedForcesMedalVetInd,
                   person.CampaignVetInd);

                person.PersonIdNo = (int)idNo.Value;
                person.PersonSeqNo = (int)seqNo.Value;

                person = context.tPERSONS.Where(p => p.PersonSeqNo == person.PersonSeqNo).FirstOrDefault();

                return person;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        #endregion

        #region Address

        public ActionResult _Address(int personId, int seqNo, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();

            ViewData["StateList"] = context.tLOCAL_CODES.Where(p => p.LocalCodeType == "ST" || p.LocalCodeType == "NA" && p.LocalCodeFromEffectDate <= effectiveDate && p.LocalCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.LocalCode, Value = p.LocalCodeIdNo.ToString() }).ToList(); //vALIDATE
            ViewData["CountryList"] = context.tLOCAL_CODES.Where(p => p.LocalCodeType == "CO" || p.LocalCodeType == "NA" && p.LocalCodeFromEffectDate <= effectiveDate && p.LocalCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.LocalCode, Value = p.LocalCodeIdNo.ToString() }).ToList(); //vALIDATE
            ViewData["CountyList"] = context.tLOCAL_CODES.Where(p => p.LocalCodeType == "CN" || p.LocalCodeType == "NA" && p.LocalCodeFromEffectDate <= effectiveDate && p.LocalCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.LocalCode, Value = p.LocalCodeIdNo.ToString() }).ToList(); //vALIDATE
            ViewData["AddressTypeList"] = context.tTYPE_CODES.Where(p => p.TypeCodeType == "AT" || p.TypeCodeType == "NA" && p.TypeCodeFromEffectDate <= effectiveDate && p.TypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.TypeCode, Value = p.TypeCodeIdNo.ToString() }).ToList(); //vALIDATE


            tPERSON_ADDRESSES add = context.tPERSON_ADDRESSES.Where(a => a.PersonAddressSeqNo == seqNo).FirstOrDefault();
            if (add == null)
            {
                add = new tPERSON_ADDRESSES();
                add.PersonIdNo = personId;
                add.PersonAddressFromEffectDate = DateTime.Today;
                add.PersonAddressToEffectDate = new DateTime(3000, 1, 1);
            }

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Address").FirstOrDefault();
            if (module != null)
            {
                ViewData["AddressDisabledFields"] = GetDisabledFields(module);
            }

            return View(add);
        }

        public ActionResult SaveAddress(tPERSON_ADDRESSES address, byte[] AddressTimestamp)
        {
            address = SaveKronosAddress(address, AddressTimestamp);
            if (address != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = address.PersonAddressSeqNo,
                    timestamp = address.Timestamp != null ? Convert.ToBase64String(address.Timestamp) : null
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tPERSON_ADDRESSES SaveKronosAddress(tPERSON_ADDRESSES address, byte[] AddressTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonAddressSeqNo", typeof(Int32));
                seqNo.Value = address.PersonAddressSeqNo;

                context.sp_saveAddressCtrl(address.PersonIdNo,
                                                    seqNo,
                                                    AddressTimestamp,
                                                    address.PersonAddressFromEffectDate,
                                                    address.PersonAddressToEffectDate,
                                                    address.PersonAddressTypeIdNo,
                                                    address.PersonAddressPrimaryInd,
                                                    address.PersonAddress1,
                                                    address.PersonAddress2,
                                                    address.PersonAddressCity,
                                                    address.PersonAddressStateProvinceIdNo,
                                                    address.PersonAddressCountryCodeIdNo,
                                                    address.PersonAddressCountyIdNo,
                                                    address.PersonAddressPostalCode);

                //   address = context.tPERSON_ADDRESSES.Where(p => p.PersonAddressSeqNo == (int)seqNo.Value).FirstOrDefault();

                return address;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        public JsonResult ReadAddress([DataSourceRequest] DataSourceRequest request, int personId)
        {
            var context = GetKronosContext();

            return Json(GetPersonAddresses(personId, context).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public List<PersonAddress> GetPersonAddresses(int personIdNo, KronosEntities context)
        {
            return context.tPERSON_ADDRESSES.Where(a => a.PersonIdNo == personIdNo).Select(a => new PersonAddress
            {
                Address = a.PersonAddress1 + " " + a.PersonAddress2,
                City = a.PersonAddressCity,
                Country = a.tLOCAL_CODES != null ? a.tLOCAL_CODES.LocalCode : string.Empty,
                PersonAddressFromEffectDate = a.PersonAddressFromEffectDate,
                PersonAddressPostalCode = a.PersonAddressPostalCode,
                PersonAddressSeqNo = a.PersonAddressSeqNo,
                PersonAddressToEffectDate = a.PersonAddressToEffectDate,
                Type = a.tTYPE_CODES != null ? a.tTYPE_CODES.TypeCode : string.Empty,
                PersonIdNo = a.PersonIdNo,
                Primary = a.PersonAddressPrimaryInd ? Resource.Yes : "No"

            }).OrderByDescending(a => a.PersonAddressFromEffectDate).ToList();
        }

        #endregion

        #region Phone

        public ActionResult _Phone(int personId, int seqNo, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();

            ViewData["PhoneTypeList"] = context.tTYPE_CODES.Where(p => p.TypeCodeType == "PT" || p.TypeCodeType == "NA" && p.TypeCodeFromEffectDate <= effectiveDate && p.TypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.TypeCode, Value = p.TypeCodeIdNo.ToString() }).ToList(); //vALIDATE


            tPERSON_PHONES phone = context.tPERSON_PHONES.Where(a => a.PersonPhoneSeqNo == seqNo).FirstOrDefault();
            if (phone == null)
            {
                phone = new tPERSON_PHONES();
                phone.PersonIdNo = personId;
                phone.PersonPhoneFromEffectDate = DateTime.Today;
                phone.PersonPhoneToEffectDate = new DateTime(3000, 1, 1);
            }

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Phone").FirstOrDefault();
            if (module != null)
            {
                ViewData["PhoneDisabledFields"] = GetDisabledFields(module);
            }

            return View(phone);
        }

        public ActionResult SavePhone(tPERSON_PHONES phone, byte[] PhoneTimestamp)
        {
            phone = SaveKronosPhone(phone, PhoneTimestamp);
            if (phone != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = phone.PersonPhoneSeqNo,
                    timestamp = phone.Timestamp != null ? Convert.ToBase64String(phone.Timestamp) : null
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tPERSON_PHONES SaveKronosPhone(tPERSON_PHONES phone, byte[] PhoneTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonPhoneSeqNo", typeof(Int32));
                seqNo.Value = phone.PersonPhoneSeqNo;

                context.sp_savePhoneCtrl(phone.PersonIdNo,
                                                seqNo,
                                                phone.PersonPhoneTypeIdNo,
                                                phone.PersonPhonePrimaryInd,
                                                phone.PersonPhoneNo,
                                                phone.PersonPhoneExt,
                                                 phone.PersonPhoneFromEffectDate,
                                                PhoneTimestamp);

                //phone = context.tPERSON_PHONES.Where(p => p.PersonPhoneSeqNo == (int)seqNo.Value).FirstOrDefault();

                return phone;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        public JsonResult ReadPhones([DataSourceRequest] DataSourceRequest request, int personId)
        {
            var context = GetKronosContext();

            return Json(GetPersonPhones(personId, context).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public List<PersonPhone> GetPersonPhones(int personIdNo, KronosEntities context)
        {
            return context.tPERSON_PHONES.Where(a => a.PersonIdNo == personIdNo).Select(a => new PersonPhone
            {
                Extension = a.PersonPhoneExt,
                Number = a.PersonPhoneNo,
                PersonIdNo = a.PersonIdNo,
                PersonPhoneFromEffectDate = a.PersonPhoneFromEffectDate,
                PersonPhoneSeqNo = a.PersonPhoneSeqNo,
                PersonPhoneToEffectDate = a.PersonPhoneToEffectDate,
                Type = a.tTYPE_CODES != null ? a.tTYPE_CODES.TypeCode : string.Empty,
                Primary = a.PersonPhonePrimaryInd ? Resource.Yes : "No"

            }).OrderByDescending(a => a.PersonPhoneFromEffectDate).ToList();
        }

        #endregion

        #region Email

        public ActionResult _Email(int personId, int seqNo, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();

            tEMAIL email = context.tEMAILs.Where(a => a.EMailSeqNo == seqNo).FirstOrDefault();
            if (email == null)
            {
                email = new tEMAIL();
                email.PersonIdNo = personId;
                email.EMailFromEffectDate = DateTime.Today;
                email.EMailToEffectDate = new DateTime(3000, 1, 1);
            }

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Email").FirstOrDefault();
            if (module != null)
            {
                ViewData["EmailDisabledFields"] = GetDisabledFields(module);
            }

            return View(email);
        }

        public ActionResult SaveEmail(tEMAIL email, byte[] EmailTimestamp)
        {
            email = SaveKronosEmail(email, EmailTimestamp);
            if (email != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = email.EMailSeqNo,
                    timestamp = email.Timestamp != null ? Convert.ToBase64String(email.Timestamp) : null
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tEMAIL SaveKronosEmail(tEMAIL email, byte[] EmailTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                context.sp_saveEmailCtrl(email.PersonIdNo, email.EMailSeqNo, email.EMailPrimaryInd ? 1 : 0, email.EMailAddress, email.EMailFromEffectDate, EmailTimestamp);

                //phone = context.tPERSON_PHONES.Where(p => p.PersonPhoneSeqNo == (int)seqNo.Value).FirstOrDefault();

                return email;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        public JsonResult ReadEmails([DataSourceRequest] DataSourceRequest request, int personId)
        {
            var context = GetKronosContext();

            return Json(GetPersonEmails(personId, context).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public List<PersonEmail> GetPersonEmails(int personIdNo, KronosEntities context)
        {
            return context.tEMAILs.Where(a => a.PersonIdNo == personIdNo).Select(a => new PersonEmail
            {
                EMailAddress = a.EMailAddress,
                EMailFromEffectDate = a.EMailFromEffectDate,
                EMailSeqNo = a.EMailSeqNo,
                EMailToEffectDate = a.EMailToEffectDate,
                PersonIdNo = a.PersonIdNo,
                Primary = a.EMailPrimaryInd ? Resource.Yes : "No"

            }).OrderByDescending(a => a.EMailFromEffectDate).ToList();
        }

        #endregion

        #region Employment Status

        public ActionResult _EmploymentStatus(int personId, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();
            tEMPLOYMENT_STATUS employmentStatus = context.tEMPLOYMENT_STATUS.Where(p => p.PersonIdNo == personId && p.EmploymentStatusFromEffectDate <= effectiveDate && p.EmploymentStatusToEffectDate >= effectiveDate).FirstOrDefault();

            if (employmentStatus == null)
            {
                employmentStatus = new tEMPLOYMENT_STATUS();
            }

            ViewData["EmployeeStatusList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "ES").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();
            ViewData["EmpoymentStatusList"] = context.tEMPLOYMENT_CODES.Where(p => p.EmploymentCodeType == "E" || p.EmploymentCodeType == "N" && p.EmploymentCodeFromEffectDate <= effectiveDate && p.EmploymentCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.EmploymentCode, Value = p.EmploymentCodeIdNo.ToString() }).ToList();
            ViewData["EmpoymentReasonList"] = context.tREASON_CODES.Where(p => p.ReasonCodeType == "ES" || p.ReasonCodeType == "NA" && p.ReasonCodeFromEffectDate <= effectiveDate && p.ReasonCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.ReasonCode, Value = p.ReasonCodeIdNo.ToString() }).ToList();
            ViewData["RecruiterList"] = context.tEMPLOYMENT_CODES.Where(p => p.EmploymentCodeType == "XXXX" || p.EmploymentCodeType == "N" && p.EmploymentCodeFromEffectDate <= effectiveDate && p.EmploymentCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.EmploymentCode, Value = p.EmploymentCodeIdNo.ToString() }).ToList();
            ViewData["SouceOfHireList"] = context.tEMPLOYMENT_CODES.Where(p => p.EmploymentCodeType == "S" || p.EmploymentCodeType == "N" && p.EmploymentCodeFromEffectDate <= effectiveDate && p.EmploymentCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.EmploymentCode, Value = p.EmploymentCodeIdNo.ToString() }).ToList();
            ViewData["I9FormOnFileList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "I9").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();
            ViewData["TerminationTypeList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "TT" || p.PersonCodeType == "NA").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Employment Status").FirstOrDefault();
            if (module != null)
            {
                ViewData["EmploymentStatusDisabledFields"] = GetDisabledFields(module);
            }

            return View(employmentStatus);
        }

        public ActionResult SaveEmploymentStatus(tEMPLOYMENT_STATUS empStat, DateTime effectiveDateEmploymentStatus, string neworupdateemploymentstatus, byte[] EmploymentStatusTimestamp)
        {
            empStat = SaveKronosEmploymentStatus(empStat, neworupdateemploymentstatus, effectiveDateEmploymentStatus, EmploymentStatusTimestamp);
            if (empStat != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = empStat.EmploymentStatusSeqNo,
                    timestamp = Convert.ToBase64String(empStat.Timestamp),
                    effectivefrom = empStat.EmploymentStatusFromEffectDate.ToShortDateString(),
                    effectiveto = empStat.EmploymentStatusToEffectDate.ToShortDateString()
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tEMPLOYMENT_STATUS SaveKronosEmploymentStatus(tEMPLOYMENT_STATUS employmentStatus, string neworupdate, DateTime? effectiveDate, byte[] EmploymentStatusTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                int? fullTime = employmentStatus.FullTimeInd ? 1 : 0;
                bool rehire = employmentStatus.Rehire != null && employmentStatus.Rehire.ToLower() == "true" ? true : false;
                double? defined4 = null;

                if (employmentStatus.EmploymentStatusUserDefined4 != null)
                {
                    defined4 = (double)employmentStatus.EmploymentStatusUserDefined4;
                }

                context.sp_saveEmploymentStatusCtrl(employmentStatus.PersonIdNo,
                                                            employmentStatus.EmploymentStatusSeqNo,
                                                            EmploymentStatusTimestamp,
                                                            employmentStatus.EmployeeStatusIdNo,
                                                            employmentStatus.EmploymentStatus,
                                                            fullTime,
                                                            employmentStatus.LastHireDate,
                                                            employmentStatus.LeaveReturnDate,
                                                            employmentStatus.I9FormOnFileIdNo,
                                                            employmentStatus.I9RenewalDate,
                                                            employmentStatus.OrgCodeIdNo,
                                                            employmentStatus.OriginalHireDate,
                                                            employmentStatus.RecruiterCode,
                                                            rehire,
                                                            employmentStatus.SeniorityDate,
                                                            employmentStatus.SourceOfHireCode,
                                                            employmentStatus.TerminationTypeIdNo,
                                                            employmentStatus.EmpNo,
                                                            employmentStatus.EmploymentStatusReasonIdNo,
                                                            employmentStatus.EmploymentStatusUserDefined1,
                                                            employmentStatus.EmploymentStatusUserDefined2,
                                                            employmentStatus.EmploymentStatusUserDefined3,
                                                            defined4,
                                                            neworupdate == "n" ? effectiveDate : employmentStatus.EmploymentStatusFromEffectDate,
                                                            employmentStatus.TerminationDate,
                                                            employmentStatus.EmploymentStatusToEffectDate,
                                                            null,
                                                            null,
                                                            null,
                                                            null);

                employmentStatus = context.tEMPLOYMENT_STATUS.Where(p => p.EmploymentStatusSeqNo == employmentStatus.EmploymentStatusSeqNo).FirstOrDefault();

                return employmentStatus;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        #endregion

        #region PersonPosition

        public ActionResult _Position(int personId, int seqNo, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();

            tPERSON_POSITIONS persPos = context.tPERSON_POSITIONS.Where(a => a.PositionSeqNo == seqNo).FirstOrDefault();
            if (persPos == null)
            {
                persPos = new tPERSON_POSITIONS();
                persPos.PersonIdNo = personId;
                persPos.PositionFromEffectDate = DateTime.Today;
                persPos.PositionToEffectDate = new DateTime(3000, 1, 1);
            }
            else
            {
                // sp_FTE_CalcAmountEmployee_Result result = context.sp_FTE_CalcAmountEmployee(persPos.PersonIdNo, seqNo, persPos.FTEHoursPerWeek, effectiveDate).FirstOrDefault();
            }

            string positionDescription = string.Empty;
            string positionTitle = string.Empty;
            string positionreportsTo = string.Empty;
            string supervisorName = string.Empty;
            string supervisorTitle = string.Empty;
            string supervisorCode = string.Empty;
            string supervisorStatus = string.Empty;
            string fteAmount = string.Empty;
            string fteAmountEmployeeTotal = string.Empty;
            string jobCode = string.Empty;
            string jobTitle = string.Empty;
            string hoursPerYear = string.Empty;
            string payGrade = string.Empty;
            string payGradeDescription = string.Empty;
            string directMaximum = string.Empty;
            string directMidpoint = string.Empty;
            string directMinimum = string.Empty;
            string totalMaximum = string.Empty;
            string totalMidpoint = string.Empty;
            string totalMinimum = string.Empty;
            string posUserDefined1 = string.Empty;
            string posUserDefined2 = string.Empty;
            string posUserDefined3 = string.Empty;
            string posUserDefined4 = string.Empty;

            if (persPos.WorkersCompensationIdNo != null)
            {
                tWCMP_WORKERS_COMPENSATION_CODES work = context.tWCMP_WORKERS_COMPENSATION_CODES.Where(w => w.WorkersCompensationIdNo == persPos.WorkersCompensationIdNo).FirstOrDefault();

                if (work != null)
                {
                    ViewData["WorkersCompensationState"] = work.LocalCodeIdNo;
                    ViewData["WorkersCompensationRate"] = work.WorkerCompRate;
                }
            }

            tPOSITION_CODES position = persPos.tPOSITION_IDS != null && persPos.tPOSITION_IDS.tPOSITION_CODES != null ? persPos.tPOSITION_IDS.tPOSITION_CODES.FirstOrDefault() : null;

            if (position != null)
            {
                positionDescription = position.PositionCode;
                positionTitle = position.PositionCodeDescription;

                hoursPerYear = position.StandardUnitsPerYear.HasValue ? position.StandardUnitsPerYear.Value.ToString("F0") : string.Empty;

                posUserDefined1 = position.PositionCodeUserDefined1;
                posUserDefined2 = position.PositionCodeUserDefined2;
                posUserDefined3 = position.PositionCodeUserDefined3.ToString();
                posUserDefined4 = position.PositionCodeUserDefined4.ToString();

                tFTE_DEFINITION_CODES fte = position.tFTE_DEFINITION_IDS != null && position.tFTE_DEFINITION_IDS.tFTE_DEFINITION_CODES != null ? position.tFTE_DEFINITION_IDS.tFTE_DEFINITION_CODES.FirstOrDefault() : null;
                if (fte != null)
                {
                    fteAmount = fte.tFTE_DEFINITION_CODE_VALUES != null && fte.tFTE_DEFINITION_CODE_VALUES.FirstOrDefault() != null && fte.tFTE_DEFINITION_CODE_VALUES.FirstOrDefault().FTEAmount.HasValue ? fte.tFTE_DEFINITION_CODE_VALUES.FirstOrDefault().FTEAmount.Value.ToString() : string.Empty;
                }
            }

            ///Get JobCode
            tJOB_CODES jobCod = context.tJOB_CODES.Where(j => j.JobCodeIdNo == position.JobCodeIdNo && j.JobCodeFromEffectDate <= effectiveDate && j.JobCodeToEffectDate >= effectiveDate).FirstOrDefault();
            if (jobCod != null)
            {
                jobCode = jobCod.JobCode;
                jobTitle = jobCod.JobCodeDescription;

                tPAY_GRADE_CODES pg = context.tPAY_GRADE_CODES.Where(p => p.PayGradeIdNo == jobCod.PayGradeIdNo && p.PayGradeCodeFromEffectDate <= effectiveDate && p.PayGradeCodeToEffectDate >= effectiveDate).FirstOrDefault();
                if (pg != null)
                {
                    payGrade = pg.PayGradeCode;
                    payGradeDescription = pg.PayGradeCodeDescription;
                    directMaximum = pg.DirectMaximumPayRate.HasValue ? pg.DirectMaximumPayRate.Value.ToString("C") : string.Empty;
                    directMinimum = pg.DirectMinimumPayRate.HasValue ? pg.DirectMinimumPayRate.Value.ToString("C") : string.Empty;
                    directMidpoint = pg.DirectMaximumPayRate.HasValue && pg.DirectMinimumPayRate.HasValue ? ((pg.DirectMaximumPayRate.Value + pg.DirectMinimumPayRate.Value) / 2).ToString("C") : string.Empty;

                    totalMaximum = pg.MaximumPayRate.HasValue ? pg.MaximumPayRate.Value.ToString("C") : string.Empty;
                    totalMinimum = pg.MinimumPayRate.HasValue ? pg.MinimumPayRate.Value.ToString("C") : string.Empty;
                    totalMidpoint = pg.MaximumPayRate.HasValue && pg.MinimumPayRate.HasValue ? ((pg.MaximumPayRate.Value + pg.MinimumPayRate.Value) / 2).ToString("C") : string.Empty;
                }
            }

            ///Get Reports To 
            tPOSITION_CODES reportTo = context.tPOSITION_CODES.Where(p => p.PositionIdNo == position.ReportToPosition && p.PositionCodeFromEffectDate <= effectiveDate && p.PositionCodeToEffectDate >= effectiveDate).FirstOrDefault();
            if (reportTo != null)
            {
                positionreportsTo = reportTo.PositionCodeDescription;
            }

            ///Get Supervisor
            tPERSON supervisor = persPos.tPERSON_IDS1 != null && persPos.tPERSON_IDS1.tPERSONS != null && persPos.tPERSON_IDS1.tPERSONS.FirstOrDefault() != null ? persPos.tPERSON_IDS1.tPERSONS.FirstOrDefault() : null;
            if (supervisor != null)
            {
                supervisorName = supervisor.FirstName.Trim() + " " + supervisor.LastName.Trim();

                ///Get Supervisor Status
                tEMPLOYMENT_STATUS supStat = context.tEMPLOYMENT_STATUS.Where(s => s.PersonIdNo == supervisor.PersonIdNo).FirstOrDefault();
                if (supStat != null && supStat.tSY_PERSON_CODES != null)
                {
                    supervisorStatus = supStat.tSY_PERSON_CODES.PersonCode;
                }

                ///Get Supervisor PositionInfo
                tPERSON_POSITIONS supPersPos = context.tPERSON_POSITIONS.Where(p => p.PersonIdNo == supervisor.PersonIdNo && p.PositionPrimaryInd == true && p.PositionFromEffectDate <= effectiveDate && p.PositionToEffectDate >= effectiveDate).FirstOrDefault();
                if (supPersPos != null)
                {
                    tPOSITION_CODES supPos = context.tPOSITION_CODES.Where(p => p.PositionIdNo == supPersPos.PositionIdNo && p.PositionCodeFromEffectDate <= effectiveDate && p.PositionCodeToEffectDate >= effectiveDate).FirstOrDefault();
                    if (supPos != null)
                    {
                        supervisorTitle = supPos.PositionCodeDescription;
                        supervisorCode = supPos.PositionCode;
                    }
                }

            }


            ViewData["PositionDescription"] = positionDescription;
            ViewData["PositionTitle"] = positionTitle;
            ViewData["PositionreportsTo"] = positionreportsTo;
            ViewData["SupervisorName"] = supervisorName;
            ViewData["SupervisorTitle"] = supervisorTitle;
            ViewData["SupervisorCode"] = supervisorCode;
            ViewData["SupervisorStatus"] = supervisorStatus;
            ViewData["FteAmount"] = fteAmount;
            ViewData["FteAmountEmployeeTotal"] = fteAmountEmployeeTotal;

            ViewData["JobCode"] = jobCode;
            ViewData["JobTitle"] = jobTitle;
            ViewData["HoursPerYear"] = hoursPerYear;
            ViewData["PayGrade"] = payGrade;
            ViewData["PayGradeDescription"] = payGradeDescription;
            ViewData["DirectMaximum"] = directMaximum;
            ViewData["DirectMidpoint"] = directMidpoint;
            ViewData["DirectMinimum"] = directMinimum;
            ViewData["TotalMaximum"] = totalMaximum;
            ViewData["TotalMidpoint"] = totalMidpoint;
            ViewData["TotalMinimum"] = totalMinimum;

            ViewData["PosUserDefined1"] = posUserDefined1;
            ViewData["PosUserDefined2"] = posUserDefined2;
            ViewData["PosUserDefined3"] = posUserDefined3;
            ViewData["PosUserDefined4"] = posUserDefined4;

            ///Dropdown datasources


            ViewData["SalariedHourlyList"] = context.tSY_PERSON_CODES.Where(p => p.PersonCodeType == "SH").Select(p => new HRISListObject { Description = p.PersonCode, Value = p.PersonCodeIdNo.ToString() }).ToList();
            ViewData["PositionReasonList"] = context.tREASON_CODES.Where(p => p.ReasonCodeType == "PP" || p.ReasonCodeType == "NA" && p.ReasonCodeFromEffectDate <= effectiveDate && p.ReasonCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.ReasonCode, Value = p.ReasonCodeIdNo.ToString() }).ToList();
            ViewData["StepList"] = context.tGED_GRADE_STEP_CODES.Where(p => p.GradeStepCodeFromEffectDate <= effectiveDate && p.GradeStepCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.GradeStepCode, Value = p.GradeStepCodeIdNo.ToString() }).ToList();
            ViewData["UnionList"] = context.tUNION_CODES.Where(p => p.UnionCodeFromEffectDate <= effectiveDate && p.UnionCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.UnionCode, Value = p.UnionCodeIdNo.ToString() }).ToList();
            ViewData["WorkersCompensationStateList"] = context.tLOCAL_CODES.Where(p => p.LocalCodeType == "ST" || p.LocalCodeType == "NA" && p.LocalCodeFromEffectDate <= effectiveDate && p.LocalCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.LocalCode, Value = p.LocalCodeIdNo.ToString() }).ToList();
            ViewData["WorkersCompensationCodeList"] = context.tWCMP_WORKERS_COMPENSATION_CODES.Where(p => p.WorkerCompFromEffectDate <= effectiveDate && p.WorkerCompToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.WorkersCompensationCode, Value = p.WorkersCompensationIdNo.ToString() }).ToList();
            ViewData["WorkersCompensationDescList"] = context.tWCMP_WORKERS_COMPENSATION_CODES.Where(p => p.WorkerCompFromEffectDate <= effectiveDate && p.WorkerCompToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.WorkersCompensationCodeDescription, Value = p.WorkersCompensationIdNo.ToString() }).ToList();

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Position").FirstOrDefault();
            if (module != null)
            {
                ViewData["DisabledFields"] = GetDisabledFields(module);
            }


            return View(persPos);
        }

        public ActionResult SavePersonPosition(tPERSON_POSITIONS persPos, DateTime effectiveDatePersonPosition, string neworupdatepersonposition, byte[] PersonPositionTimestamp)
        {
            persPos = SaveKronosPersonPosition(persPos, neworupdatepersonposition, effectiveDatePersonPosition, PersonPositionTimestamp);
            if (persPos != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = persPos.PositionSeqNo,
                    timestamp = persPos.Timestamp != null ? Convert.ToBase64String(persPos.Timestamp) : null,
                    effectivefrom = persPos.PositionFromEffectDate.ToShortDateString(),
                    effectiveto = persPos.PositionToEffectDate.ToShortDateString()
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tPERSON_POSITIONS SaveKronosPersonPosition(tPERSON_POSITIONS persPos, string neworupdate, DateTime? effectiveDate, byte[] EmploymentStatusTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                int? posPrimaryInd = persPos.PositionPrimaryInd ? 1 : 0;
                int? promotionInd = persPos.PromotionInd ? 1 : 0;
                int? exempStatusInd = persPos.ExemptStatusInd ? 1 : 0;
                int? seasonalWorkerInd = persPos.SeasonalWorkerInd ? 1 : 0;

                double? defined2 = null;
                if (persPos.PositionUserDefined2.HasValue)
                {
                    defined2 = (double)persPos.PositionUserDefined2;
                }

                int? primary = persPos.PositionPrimaryInd ? 1 : 0;
                int? promotion = persPos.PromotionInd ? 1 : 0;
                int? exemp = persPos.ExemptStatusInd ? 1 : 0;
                int? seasonal = persPos.SeasonalWorkerInd ? 1 : 0;

                double? userDefined2 = null;
                if (persPos.PositionUserDefined2.HasValue)
                {
                    userDefined2 = (double)persPos.PositionUserDefined2.Value;
                }

                context.sp_savePositionCtrl(persPos.PersonIdNo,
                                                   persPos.PositionIdNo,
                                                   persPos.PositionSeqNo,
                                                   persPos.Timestamp,
                                                   persPos.PositionReasonIdNo,
                                                   neworupdate == "n" ? effectiveDate : persPos.PositionFromEffectDate,
                                                   new DateTime(3000, 01, 01),
                                                   persPos.SupervisorIdNo,
                                                   persPos.PositionStartDate,
                                                   persPos.Title,
                                                   persPos.MailStop,
                                                   primary,
                                                   promotion,
                                                   exemp,
                                                   persPos.SalariedHourlyCodeIdNo,
                                                   seasonal,
                                                   persPos.UnionCodeIdNo,
                                                   persPos.UnionDate,
                                                   persPos.PositionUserDefined1,
                                                   userDefined2,
                                                   persPos.FTEHoursPerWeek,
                                                   persPos.WorkersCompensationIdNo,
                                                   persPos.CompLocationFactor,
                                                   null,
                                                   null,
                                                   null,
                                                   null);
                persPos = context.tPERSON_POSITIONS.Where(p => p.PositionSeqNo == (int)persPos.PositionSeqNo).FirstOrDefault();

                return persPos;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        public JsonResult ReadPersonPositions([DataSourceRequest] DataSourceRequest request, int personId, DateTime effectiveDate)
        {
            var context = GetKronosContext();

            return Json(GetPersonPositions(personId, effectiveDate, context).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        private List<PersonPosition> GetPersonPositions(int personId, DateTime effectiveDate, KronosEntities context)
        {

            List<tPERSON_POSITIONS> ppList = context.tPERSON_POSITIONS.Where(p => p.PersonIdNo == personId && p.PositionFromEffectDate <= effectiveDate && p.PositionToEffectDate >= effectiveDate).OrderBy(p => p.PositionFromEffectDate).ToList();
            List<PersonPosition> modelList = new List<PersonPosition>();

            foreach (tPERSON_POSITIONS item in ppList)
            {
                PersonPosition model = new PersonPosition();
                model.Primary = item.PositionPrimaryInd ? Resource.Yes : "No";
                model.PersonPositionSeqNo = item.PositionSeqNo;
                model.PersonIdNo = item.PersonIdNo;
                tPOSITION_CODES pos = item.tPOSITION_IDS != null && item.tPOSITION_IDS.tPOSITION_CODES != null ? item.tPOSITION_IDS.tPOSITION_CODES.FirstOrDefault() : null;
                if (pos != null)
                {
                    model.PositionCode = pos.PositionCode;
                    model.PositionDescription = pos.PositionCodeDescription;
                    model.JobCode = pos.tJOB_CODE_IDS != null && pos.tJOB_CODE_IDS.tJOB_CODES != null && pos.tJOB_CODE_IDS.tJOB_CODES.FirstOrDefault() != null ? pos.tJOB_CODE_IDS.tJOB_CODES.FirstOrDefault().JobCode : string.Empty;
                }


                modelList.Add(model);
            }

            return modelList;
        }

        public JsonResult ReadWorkersCompensationCodes(int? localCodeIdNo)
        {
            if (localCodeIdNo == null)
            {
                localCodeIdNo = 0;
            }
            var context = GetKronosContext();

            return Json(context.tWCMP_WORKERS_COMPENSATION_CODES.Where(p => p.WorkerCompFromEffectDate <= DateTime.Today && p.WorkerCompToEffectDate >= DateTime.Today && p.LocalCodeIdNo == localCodeIdNo).Select(p => new HRISListObject { Description = p.WorkersCompensationCode, Value = p.WorkersCompensationIdNo.ToString() }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReadWorkersCompensationDescriptions(int? localCodeIdNo)
        {
            if (localCodeIdNo == null)
            {
                localCodeIdNo = 0;
            }

            var context = GetKronosContext();

            return Json(context.tWCMP_WORKERS_COMPENSATION_CODES.Where(p => p.WorkerCompFromEffectDate <= DateTime.Today && p.WorkerCompToEffectDate >= DateTime.Today && p.LocalCodeIdNo == localCodeIdNo).Select(p => new HRISListObject { Description = p.WorkersCompensationCodeDescription, Value = p.WorkersCompensationIdNo.ToString() }).ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Pay Status

        public ActionResult _PayStatus(int personId, int seqNo, DateTime effectiveDate, int positionId)
        {
            KronosEntities context = GetKronosContext();

            tPAY_STATUS payStatus = context.tPAY_STATUS.Where(a => a.PayStatusSeqNo == seqNo).FirstOrDefault();
            if (payStatus == null)
            {
                payStatus = new tPAY_STATUS();
                payStatus.PersonIdNo = personId;
                payStatus.PayStatusFromEffectDate = DateTime.Today;
                payStatus.PayStatusToEffectDate = new DateTime(3000, 1, 1);
                payStatus.PositionIdNo = positionId;
            }

            string annualSalary = string.Empty;
            string lastChangeAmount = string.Empty;
            string lastChangePercent = string.Empty;


            if (payStatus.tSY_PAY_RATE_FREQUENCY_CODES1 != null && payStatus.tSY_PAY_RATE_FREQUENCY_CODES1.AnnualizingFactor != null)
            {
                annualSalary = ((decimal)payStatus.tSY_PAY_RATE_FREQUENCY_CODES1.AnnualizingFactor * payStatus.PayRate).ToString("F2");
            }

            if (payStatus.LastAnnualizedPayRate != 0 && payStatus.AnnualizingFactor != 0)
            {
                lastChangeAmount = (payStatus.PayRate - (payStatus.LastAnnualizedPayRate / payStatus.AnnualizingFactor)).ToString("F2");

                if (payStatus.PayRate != 0)
                {
                    lastChangePercent = ((((payStatus.PayRate * payStatus.AnnualizingFactor) - payStatus.LastAnnualizedPayRate) / payStatus.LastAnnualizedPayRate) * 100).ToString("F2");
                }
            }


            ViewData["AnnualSalary"] = annualSalary;
            ViewData["LastChangeAmount"] = lastChangeAmount;
            ViewData["LastChangePercent"] = lastChangePercent;

            ///Dropdown datasources
            ViewData["CompensationTypeList"] = context.tTYPE_CODES.Where(p => p.TypeCodeType == "CT" && p.TypeCodeFromEffectDate <= effectiveDate && p.TypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.TypeCode, Value = p.TypeCodeIdNo.ToString() }).ToList();
            ViewData["FrequencyeList"] = context.tSY_PAY_RATE_FREQUENCY_CODES.Select(f => new FrequencyInfo { AnnualizingFactor = f.AnnualizingFactor, PayRateFrequencyCode = f.PayRateFrequencyCode, PayRateFrequencyCodeIdNo = f.PayRateFrequencyCodeIdNo }).ToList();
            ViewData["ShiftList"] = context.tSHIFT_DIFFERENTIAL_CODES.Select(p => new HRISListObject { Description = p.ShiftDiffCode, Value = p.ShiftDiffIdNo.ToString() }).ToList();
            ViewData["PayReasonList"] = context.tREASON_CODES.Where(p => p.ReasonCodeType == "PS" || p.ReasonCodeType == "NA" && p.ReasonCodeFromEffectDate <= effectiveDate && p.ReasonCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.ReasonCode, Value = p.ReasonCodeIdNo.ToString() }).ToList();
            ViewData["CurrencyList"] = context.tCURRENCY_CODES.Select(p => new HRISListObject { Description = p.CurrencyCode, Value = p.CurrencyCodeIdNo.ToString() }).ToList();

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Pay Status").FirstOrDefault();
            if (module != null)
            {
                ViewData["PayStatusDisabledFields"] = GetDisabledFields(module);
            }

            return View(payStatus);
        }

        public ActionResult SavePayStatus(tPAY_STATUS ps, DateTime effectiveDatePayStatus, string neworupdatepaystatus, byte[] PayStatusTimestamp)
        {
            ps = SaveKronosPayStatus(ps, neworupdatepaystatus, effectiveDatePayStatus, PayStatusTimestamp);
            if (ps != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = ps.PayStatusSeqNo,
                    timestamp = ps.Timestamp != null ? Convert.ToBase64String(ps.Timestamp) : null,
                    effectivefrom = ps.PayStatusFromEffectDate.ToShortDateString(),
                    effectiveto = ps.PayStatusToEffectDate.ToShortDateString()
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tPAY_STATUS SaveKronosPayStatus(tPAY_STATUS ps, string neworupdate, DateTime? effectiveDate, byte[] PayStatusTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                int? basePay = ps.BasePayInd ? 1 : 0;

                double? unitsPerWeek = null;
                if (ps.StandardUnitsPerWeek.HasValue)
                {
                    unitsPerWeek = (double)ps.StandardUnitsPerWeek.Value;
                }

                double? userDefined2 = null;
                if (ps.PayStatusUserDefined2.HasValue)
                {
                    userDefined2 = (double)ps.PayStatusUserDefined2.Value;
                }

                System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PayStatusSeqNo", typeof(Int32));
                seqNo.Value = ps.PayStatusSeqNo;

                context.sp_savePayStatusCtrl(ps.PersonIdNo,
                                                   ps.PayStatusSeqNo,
                                                   PayStatusTimestamp,
                                                   neworupdate == "n" ? effectiveDate : ps.PayStatusFromEffectDate,
                                                   ps.PayStatusToEffectDate,
                                                   ps.PositionIdNo,///Position ==-1 means its not a new Hire so we use the same positionId else its a new hire and we received the positionIdNo as parameter
                                                   ps.PayReasonIdNo,
                                                   ps.PayStatusCompensationTypeIdNo,
                                                   ps.PayRateFrequencyCodeIdNo,
                                                   (double)ps.PayRate,
                                                   unitsPerWeek,
                                                   ps.AnnualizingFactor,
                                                   null,
                                                   ps.LastAnnualizedPayRate,
                                                   basePay,
                                                   ps.LastSalaryChangeDate,
                                                   ps.NextPayReviewDate,
                                                   ps.ShiftDiffIdNo,
                                                   ps.CurrencyCodeIdNo,
                                                   ps.PayStatusUserDefined1,
                                                   userDefined2,
                                                   null,
                                                   null,
                                                   null,
                                                   null,
                                                   null,
                                                   null);

                if (ps.PayStatusSeqNo == 0)
                {
                    ps = context.tPAY_STATUS.Where(p => p.PersonIdNo == ps.PersonIdNo).OrderByDescending(p => p.PayStatusSeqNo).FirstOrDefault();
                }
                else
                {
                    ps = context.tPAY_STATUS.Where(p => p.PayStatusSeqNo == ps.PayStatusSeqNo).FirstOrDefault();
                }

                return ps;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        public JsonResult ReadPayStatus([DataSourceRequest] DataSourceRequest request, int personId, DateTime effectiveDate, int positionId)
        {
            var context = GetKronosContext();

            return Json(GetPayStatus(personId, effectiveDate, positionId, context).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        private List<PersonPayStatus> GetPayStatus(int personId, DateTime effectiveDate, int positionId, KronosEntities context)
        {

            List<tPAY_STATUS> psList = context.tPAY_STATUS.Where(p => p.PersonIdNo == personId && p.PositionIdNo == positionId && p.PayStatusFromEffectDate <= effectiveDate && p.PayStatusToEffectDate >= effectiveDate).OrderBy(p => p.PayStatusFromEffectDate).ToList();
            List<PersonPayStatus> modelList = new List<PersonPayStatus>();

            foreach (tPAY_STATUS item in psList)
            {
                PersonPayStatus model = new PersonPayStatus
                {
                    AnnualSalary = item.tSY_PAY_RATE_FREQUENCY_CODES1 != null && item.tSY_PAY_RATE_FREQUENCY_CODES1.AnnualizingFactor != null ? ((decimal)item.tSY_PAY_RATE_FREQUENCY_CODES1.AnnualizingFactor * item.PayRate).ToString("C") : string.Empty,
                    CompensationType = item.tTYPE_CODES != null ? item.tTYPE_CODES.TypeCode : string.Empty,
                    EffectiveFrom = item.PayStatusFromEffectDate.ToShortDateString(),
                    Frequency = item.tSY_PAY_RATE_FREQUENCY_CODES1 != null ? item.tSY_PAY_RATE_FREQUENCY_CODES1.PayRateFrequencyCode : string.Empty,
                    LastChangeAmount = item.LastAnnualizedPayRate != 0 && item.AnnualizingFactor != 0 ? (item.PayRate - (item.LastAnnualizedPayRate / item.AnnualizingFactor)).ToString("F2") : string.Empty,
                    LastChangeDate = item.LastSalaryChangeDate.HasValue ? item.LastSalaryChangeDate.Value.ToShortDateString() : string.Empty,
                    NextChangeDate = item.NextPayReviewDate.HasValue ? item.NextPayReviewDate.Value.ToShortDateString() : string.Empty,
                    PayRate = item.PayRate.ToString("C"),
                    PayStatusSeqNo = item.PayStatusSeqNo,
                    PersonIdNo = item.PersonIdNo,
                    PositionIdNo = item.PositionIdNo,
                    Reason = item.tREASON_CODES != null ? item.tREASON_CODES.ReasonCode : string.Empty
                };

                modelList.Add(model);
            }

            return modelList;
        }

        #endregion

        #region Payroll Status

        public ActionResult _PayrollStatus(int personId, DateTime effectiveDate)
        {
            KronosEntities context = GetKronosContext();

            tPERSON_PAYROLL_SETUPS payrollStatus = context.tPERSON_PAYROLL_SETUPS.Where(p => p.PersonIdNo == personId && p.PersonPayrollSetupStartDate <= effectiveDate && p.PersonPayrollSetupEndDate >= effectiveDate).FirstOrDefault();
            if (payrollStatus == null)
            {
                payrollStatus = new tPERSON_PAYROLL_SETUPS();
                payrollStatus.PersonIdNo = personId;
                payrollStatus.PersonPayrollSetupStartDate = DateTime.Today;
                payrollStatus.PersonPayrollSetupEndDate = new DateTime(3000, 1, 1);
            }

            int employeeClassificationIdNo = 0;
            int employeeClassificationSeqNo = 0;
            string employeeClassificationTimestamp = string.Empty;

            int exemptStatusIdNo = 0;
            int exemptStatusSeqNo = 0;
            string exemptStatusTimestamp = string.Empty;

            List<tPERSON_PAYROLL_CODE_EE_TYPES> payroollCodeTypeList = context.tPERSON_PAYROLL_CODE_EE_TYPES.Where(t => t.PersonIdNo == personId && t.PayGroupIdNo == payrollStatus.PayGroupIdNo).ToList();
            foreach (tPERSON_PAYROLL_CODE_EE_TYPES item in payroollCodeTypeList)
            {
                if (item.tEMPLOYER_EMPLOYEE_TYPE_CODES != null && item.tEMPLOYER_EMPLOYEE_TYPE_CODES.EmployerEmployeeTypeCodeType == "EC")
                {
                    employeeClassificationIdNo = item.PersonPayrollCodeEETypeCodeIdNo;
                    employeeClassificationSeqNo = item.PersonPayrollCodeEETypeSeqNo;
                    employeeClassificationTimestamp = item.PersonPayrollCodeEETypeTimestamp != null ? Convert.ToBase64String(item.PersonPayrollCodeEETypeTimestamp) : string.Empty;
                }

                if (item.tEMPLOYER_EMPLOYEE_TYPE_CODES != null && item.tEMPLOYER_EMPLOYEE_TYPE_CODES.EmployerEmployeeTypeCodeType == "EE")
                {
                    exemptStatusIdNo = item.PersonPayrollCodeEETypeCodeIdNo;
                    exemptStatusSeqNo = item.PersonPayrollCodeEETypeSeqNo;
                    exemptStatusTimestamp = item.PersonPayrollCodeEETypeTimestamp != null ? Convert.ToBase64String(item.PersonPayrollCodeEETypeTimestamp) : string.Empty;
                }
            }

            ViewData["EmployeeClassificationIdNo"] = employeeClassificationIdNo;
            ViewData["EmployeeClassificationSeqNo"] = employeeClassificationSeqNo;
            ViewData["EmployeeClassificationTimestamp"] = employeeClassificationTimestamp;
            ViewData["ExemptStatusIdNo"] = exemptStatusIdNo;
            ViewData["ExemptStatusSeqNo"] = exemptStatusSeqNo;
            ViewData["ExemptStatusTimestamp"] = exemptStatusTimestamp;

            ///Dropdown datasources
            ViewData["PayrollStatusList"] = context.tPAYROLL_STATUS_CODES.Where(p => p.PayrollStatusCodeType == "PS" && p.PayrollStatusCodeFromEffectDate <= effectiveDate && p.PayrollStatusCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.PayrollStatusCode, Value = p.PayrollStatusCodeIdNo.ToString() }).OrderBy(d => d.Description).ToList();
            ViewData["EmpClassificationList"] = context.tEMPLOYER_EMPLOYEE_TYPE_CODES.Where(p => p.EmployerEmployeeTypeCodeType == "EC" || p.EmployerEmployeeTypeCodeType == "NA" && p.EmployerEmployeeTypeCodeFromEffectDate <= effectiveDate && p.EmployerEmployeeTypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.EmployerEmployeeTypeCode, Value = p.EmployerEmployeeTypeCodeIdNo.ToString() }).OrderBy(d => d.Description).ToList();
            ViewData["TaxStatusList"] = context.tEMPLOYER_EMPLOYEE_TYPE_CODES.Where(p => p.EmployerEmployeeTypeCodeType == "EE" || p.EmployerEmployeeTypeCodeType == "NA" && p.EmployerEmployeeTypeCodeFromEffectDate <= effectiveDate && p.EmployerEmployeeTypeCodeToEffectDate >= effectiveDate).Select(p => new HRISListObject { Description = p.EmployerEmployeeTypeCode, Value = p.EmployerEmployeeTypeCodeIdNo.ToString() }).OrderBy(d => d.Description).ToList();

            ViewData["PayrollSetupValuesList"] = context.tPERSON_PAYROLL_SETUP_VALUES.Where(p => p.PersonIdNo == personId && p.PayGroupIdNo == payrollStatus.PayGroupIdNo).Select(p =>
            new PayrollSetupValuesModel
            {
                Description = p.tPAYROLL_PROPERTY_CODES.PayrollPropertyCode,
                Value = p.tPAYROLL_PROPERTY_CODES != null && p.tPAYROLL_PROPERTY_CODES.tPAYROLL_TYPE_CODES != null && p.tPAYROLL_PROPERTY_CODES.tPAYROLL_TYPE_CODES.PayrollTypeCode != null && p.tPAYROLL_PROPERTY_CODES.tPAYROLL_TYPE_CODES.PayrollTypeCode.ToLower() == "indicator" ? (p.PersonPayrollSetupValue == "0" ? "N" : "Y") : p.PersonPayrollSetupValue,
                Type = p.tPAYROLL_PROPERTY_CODES.tPAYROLL_TYPE_CODES.PayrollTypeCode,
                PersonPayrollSetupValueSeqNo = p.PersonPayrollSetupValueSeqNo,
                FromEffectiveDate = p.PersonPayrollSetupValueStartDate,
                ToEffectiveDate = p.PersonPayrollSetupValueEndDate
            }).OrderBy(m => m.Description).ToList();

            HRISEntities hrisContext = GetHrisContext();

            Module module = hrisContext.Modules.Where(m => m.Name == "Payroll Status").FirstOrDefault();
            if (module != null)
            {
                ViewData["PayStatusDisabledFields"] = GetDisabledFields(module);
            }

            return View(payrollStatus);
        }

        public ActionResult SavePayrollStatus(tPERSON_PAYROLL_SETUPS ps, DateTime effectiveDatePayroll, string neworupdatepayroll, byte[] PayrollTimestamp, int EmployeeClassificationIdNo, int EmployeeClassificationSeqNo, byte[] EmployeeClassificationTimestamp, int ExemptStatusIdNo, int ExemptStatusSeqNo, byte[] ExemptStatusTimestamp)
        {
            ps = SaveKronosPayrollStatus(ps, neworupdatepayroll, effectiveDatePayroll, PayrollTimestamp, EmployeeClassificationIdNo, EmployeeClassificationSeqNo, EmployeeClassificationTimestamp, ExemptStatusIdNo, ExemptStatusSeqNo, ExemptStatusTimestamp);
            if (ps != null)
            {
                return Json(new
                {
                    status = "success",
                    seqNo = ps.PersonPayrollSetupSeqNo,
                    timestamp = ps.PersonPayrollSetupTimestamp != null ? Convert.ToBase64String(ps.PersonPayrollSetupTimestamp) : null,
                    effectivefrom = ps.PersonPayrollSetupStartDate.ToShortDateString(),
                    effectiveto = ps.PersonPayrollSetupEndDate.ToShortDateString()
                });
            }
            else
            {
                return Json(new
                {
                    status = "error"
                });
            }
        }

        public tPERSON_PAYROLL_SETUPS SaveKronosPayrollStatus(tPERSON_PAYROLL_SETUPS ps, string neworupdate, DateTime? effectiveDate, byte[] PayrollTimestamp, int EmployeeClassificationIdNo, int EmployeeClassificationSeqNo, byte[] EmployeeClassificationTimestamp, int ExemptStatusIdNo, int ExemptStatusSeqNo, byte[] ExemptStatusTimestamp)
        {
            try
            {
                KronosEntities context = GetKronosContext();

                System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("SeqNo", typeof(Int32));
                seqNo.Value = ps.PersonPayrollSetupSeqNo;

                context.sp_saveEmpPaySetupCtrl(
                    ps.PersonIdNo,
                   0,
                   seqNo,
                   neworupdate == "n" ? effectiveDate : ps.PersonPayrollSetupStartDate,
                   ps.PersonPayrollSetupEndDate,
                   null,
                   ps.PayGroupIdNo,
                   ps.ResidenceLocationIdNo,
                   ps.BenefitPayGroupInd,
                   ps.PayrollStatusCodeIdNo,
                   EmployeeClassificationIdNo,
                   EmployeeClassificationSeqNo,
                   EmployeeClassificationTimestamp,
                   ExemptStatusIdNo,
                   ExemptStatusSeqNo,
                   ExemptStatusTimestamp,
                   PayrollTimestamp
                    );

                if (seqNo.Value.ToString() == "0")
                {
                    ps = context.tPERSON_PAYROLL_SETUPS.Where(p => p.PersonIdNo == ps.PersonIdNo).OrderByDescending(p => p.PersonPayrollSetupSeqNo).FirstOrDefault();
                }
                else
                {
                    ps = context.tPERSON_PAYROLL_SETUPS.Where(p => p.PersonPayrollSetupSeqNo == ps.PersonPayrollSetupSeqNo).FirstOrDefault();
                }

                return ps;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }


        #endregion


        #region Shared Functions

        private string GetDisabledFields(Module module)
        {
            List<string> fields;

            if (DoesLoggedUserHasFullAccess())
            {
                fields = module.ModuleScreenFields.Where(f => f.DisabledByKronos.HasValue && f.DisabledByKronos.Value).Select(f => "#" + f.FieldName).ToList();
            }
            else
            {
                fields = module.ModuleScreenFields.Where(f => f.Enabled == false).Select(f => "#" + f.FieldName).ToList();
            }

            return String.Join(",", fields);
        }

        #endregion
    }
}