﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRIS.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;

namespace HRIS.Controllers
{
    public class ProcessedFilesController : BaseController
    {

        // GET: ProcessedFiles
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchFiles([DataSourceRequest] DataSourceRequest request, DateTime? fromDate, DateTime? toDate, bool showOnlyError, string employeeId)
        {
            string seedEmpId = "@@##$$%%";
            if(string.IsNullOrEmpty(employeeId))
            {
                employeeId = seedEmpId;
            }
            DateTime seedDate = new DateTime(1900, 1, 1);
            if (fromDate == null)
            {
                fromDate = seedDate;
            }

            if (toDate == null)
            {
                toDate = seedDate;
            }

            var context = GetHrisContext();

            List<ProcessedFilesModel> list = context.ExtractedFiles.Where(f => (f.Date >= fromDate || fromDate == seedDate) && (f.Date <= toDate || toDate == seedDate)&&(f.ExtractedFileModules.Where(s=>s.EmpId == employeeId).Count()>0 || employeeId == seedEmpId)).Select(f =>
            new ProcessedFilesModel
            {
                Date = f.Date,
                FileName = f.Name,
                Id = f.Id,
                Status = f.Status,
                ModuleCount = f.ExtractedFileModules.Count,
                SuccessCount = f.ExtractedFileModules.Where(m => m.Status == "OK").Count(),
                WarningCount = f.ExtractedFileModules.Where(m => m.Status == "warning").Count(),
                ErrorCount = f.ExtractedFileModules.Where(m => m.Status == "error").Count()

            }).ToList();

            if (showOnlyError)
            {
                list = list.Where(l => l.ErrorWarningCount > 0).ToList();
            }

            return Json(list.OrderByDescending(f=>f.Date).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReadFileModules([DataSourceRequest] DataSourceRequest request, int fileId)
        {
            var context = GetHrisContext();
            List<ProcessedFileModulesModel> list = context.ExtractedFileModules.Where(f => f.ExtractedFileId == fileId).Select(f =>
              new ProcessedFileModulesModel
              {
                  EmployeeId = f.EmpId,
                  Id = f.Id,
                  Info = f.Info,
                  Log = f.Log,
                  ModuleId = f.ModuleId,
                  ModuleName = f.Module.Name,
                  Status = f.Status

              }).ToList();

            foreach (ProcessedFileModulesModel item in list)
            {
                if (!string.IsNullOrEmpty(item.Log))
                {
                    string[] errors = item.Log.Split('#');

                    string log = string.Empty;

                    if (errors.Length > 0)
                    {
                        log += "<ul>";
                        foreach (string mess in errors)
                        {
                            if (mess.Trim() != string.Empty)
                            {
                                log += "<li>" + mess + "</li>";
                            }
                        }

                        log += "</ul>";
                    }

                    item.Log = log;
                }
            }

            return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RedirectToEmployee(string empNo, string changes)
        {
            var context = GetKronosContext();

            DateTime effectiveDate = DateTime.Today;

            if(!string.IsNullOrEmpty(changes))
            {
                string[] array = changes.Split('|');
                if(array.Length>3)
                {
                    DateTime.TryParse(array[3], out effectiveDate);
                }
            }

            if(effectiveDate == null)
            {
                effectiveDate = DateTime.Today;
            }

            tEMPLOYMENT_STATUS status = context.tEMPLOYMENT_STATUS.Where(e => e.EmpNo == empNo).FirstOrDefault();


            TempData["PersonIdNo"] = status.PersonIdNo;
            TempData["EffectiveDate"] = effectiveDate;

            return Json(new { status = "success" });
        }
    }


}
