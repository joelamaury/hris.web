﻿using HRIS.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRIS.Controllers
{
    public class EmployeeSearchController : BaseController
    {
        // GET: EmployeeSearch
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchEmployees([DataSourceRequest] DataSourceRequest request, DateTime effectiveDate, string firstName, string lastName, string employeeId, string positionTitle, string supervisorLastName, string status, string socialsecurity)
        {

            var context = GetKronosContext();

            List<EmployeeSearch> list = context.sp_findEmployeesUserAccess(firstName.Trim() + "%", lastName.Trim() + "%", employeeId.Trim() + "%", socialsecurity.Trim() + "%", status == string.Empty ? "%" : status, effectiveDate, false, supervisorLastName.Trim() + "%", positionTitle.Trim() + "%").Select(e => new EmployeeSearch
            {
                EffectiveDate = effectiveDate.ToShortDateString(),
                EmployeeFullName = e.LastName + ", " + e.FirstName + " " + e.MiddleName,
                EmployeeId = e.EmployeeId,
                Organizations = e.Organizations,
                PK1 = e.PK1,
                PK2 = e.PK2,
                PositionTitle = e.PositionTitle,
                SSN = e.SSN,
                Status = e.Status,
                SupervisorFullName = e.SupervisorLastName != string.Empty && e.SupervisorFirstName != string.Empty ? (e.SupervisorLastName + ", " + e.SupervisorFirstName + " " + e.SupervisorMiddleName) : string.Empty,
                IsPrimaryPosition = e.IsPrimaryPosition

            }).ToList();

            foreach (EmployeeSearch child in list)
            {
                child.OrganizationText = String.Join("/", list.Where(c => c.PK1 == child.PK1 && c.PK2 == child.PK2 && child.PositionTitle == c.PositionTitle).OrderBy(c => c.Status).Select(c => c.Organizations));

                if (child.Status != null)
                {
                    string[] st = child.Status.Split('|');
                    if (st.Length > 1)
                    {
                        child.StatusText = st[1];
                    }
                }

                if (child.SupervisorFullName.Trim() == ",")
                {
                    child.SupervisorFullName = string.Empty;
                }

                ///Mark item duplicated if results as employee with more than 1 position
                child.Dupilcated = list.Where(x => x.EmployeeId == child.EmployeeId && x.PK1 == child.PK1 && x.PK2 == child.PK2).Select(x => x.PositionTitle).Distinct().Count() > 1 ? true : false;

            }

            foreach (EmployeeSearch child in list.Where(e => e.Dupilcated).ToList())
            {
                ///Search for the first primary position for fuplicated employee
                string posTit = list.Where(e => e.EmployeeId == child.EmployeeId && e.IsPrimaryPosition == "1").Select(e => e.PositionTitle).FirstOrDefault();

                ///If not [romary position found, set the first position found as primary
                if (string.IsNullOrEmpty(posTit))
                {
                    posTit = list.Where(e => e.EmployeeId == child.EmployeeId).Select(e => e.PositionTitle).FirstOrDefault();
                }

                child.PositionTitle = posTit;
            }

            return Json(list.Select(e => new { e.EffectiveDate, e.EmployeeFullName, e.EmployeeId, e.PK1, e.PK2, e.PositionTitle, e.SSN, e.StatusText, e.SupervisorFullName, e.OrganizationText }).Distinct().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RedirectToEmployee(int personIdNo, DateTime effectiveDate)
        {
            TempData["PersonIdNo"] = personIdNo;
            TempData["EffectiveDate"] = effectiveDate;

            return Json(new { status = "success" });
        }
    }
}