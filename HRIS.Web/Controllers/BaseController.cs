﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Threading;
using System.Globalization;
using HRIS.Helpers;
using NLog;
using Models;

namespace HRIS.Controllers
{
    public class BaseController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public KronosEntities GetKronosContext()
        {
            return new KronosEntities();
        }

        public HRISEntities GetHrisContext()
        {
            return new HRISEntities();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int culture = 0;
            if (this.Session == null || this.Session["CurrentCulture"] == null)
            {
                if (Request.Cookies["HRISLanguage"] == null)
                {
                    HttpCookie langCookie = new HttpCookie("HRISLanguage");
                    langCookie.Value = culture.ToString();
                    langCookie.Expires = DateTime.Today.AddYears(10);
                    Response.Cookies.Add(langCookie);
                }
                else
                {
                    int.TryParse(Request.Cookies["HRISLanguage"].Value, out culture);
                }
                this.Session["CurrentCulture"] = culture;
            }
            else
            {
                culture = (int)this.Session["CurrentCulture"];
            }
            //Cultura actual.VF.    
            CultureHelper.CurrentCulture = culture;

            HttpSessionStateBase session = filterContext.HttpContext.Session;
            // If the browser session or authentication session has expired...
            if (session.IsNewSession || Session["user"] == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, return result as a simple string, 
                    // and inform calling JavaScript code that a user should be redirected.
                    JsonResult result = Json("SessionTimeout", "text/html");
                    filterContext.Result = result;
                }
                else
                {
                    if (!((filterContext.ActionDescriptor.ActionName == "Index") && filterContext.Controller.GetType().ToString() == "HRIS.Controllers.LoginController"))
                    {
                        // For round-trip requests,
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Login" }, { "Action", "Index" } });
                    }
                }
            }
            else
            {
                bool hasPermission = true;

                switch (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName)
                {
                    case "ProcessedFiles":
                        {
                            if (Session["ProcessedFilesPermission"] == null || ((bool)Session["ProcessedFilesPermission"]) == false)
                            {
                                hasPermission = false;
                            }
                            break;
                        }

                    case "EmployeeSearch":
                    case "Employee":
                        {
                            if (Session["SearchEmployeePermission"] == null || ((bool)Session["SearchEmployeePermission"]) == false)
                            {
                                hasPermission = false;
                            }
                            break;
                        }

                    case "Configuration":
                        {
                            if (filterContext.ActionDescriptor.ActionName == "ScreenFields" || filterContext.ActionDescriptor.ActionName == "_Fields")
                            {
                                if (Session["ScreenFieldsPermission"] == null || ((bool)Session["ScreenFieldsPermission"]) == false)
                                {
                                    hasPermission = false;
                                }
                            }
                            if (filterContext.ActionDescriptor.ActionName == "ProfilePermissions" || filterContext.ActionDescriptor.ActionName == "_Sections")
                            {
                                if (Session["ProfilePermissions"] == null || ((bool)Session["ProfilePermissions"]) == false)
                                {
                                    hasPermission = false;
                                }
                            }
                            break;
                        }
                }

                if(hasPermission == false)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Base" }, { "Action", "AccessDenied" } });
                }

            }
        }

        public bool DoesLoggedUserHasFullAccess()
        {
            return (bool)Session["ApplicationFullAccessPermission"];
        }

        public ActionResult Logout()
        {
            Session["user"] = null;

            return RedirectToAction("Index", "Login");
        }

        public JsonResult ChangeCulture(int language)
        {
            if (language == 0)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            }
            else if (language == 1)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-US");
            }
            else
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            }

            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            Session["CurrentCulture"] = language;

            if (Request.Cookies["HRISLanguage"] == null)
            {
                HttpCookie langCookie = new HttpCookie("HRISLanguage");
                langCookie.Value = language.ToString();
                langCookie.Expires = DateTime.Today.AddYears(10);
                Response.Cookies.Add(langCookie);
            }
            else
            {
                HttpCookie langCookie = Request.Cookies["HRISLanguage"];
                langCookie.Value = language.ToString();
                langCookie.Expires = DateTime.Today.AddYears(10);
                Response.Cookies.Set(langCookie);
            }

            return Json(new { status = "success" });
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception e = filterContext.Exception;
            logger.Error(e);
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}
