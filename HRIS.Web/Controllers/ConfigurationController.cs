﻿using HRIS.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace HRIS.Controllers
{
    public class ConfigurationController : BaseController
    {
        #region Screen Fields

        public ActionResult ScreenFields()
        {
            var context = GetHrisContext();

            ViewData["ModuleList"] = context.Modules.Select(m => new HRISListObject { Description = m.Name, Value = m.Id.ToString() }).ToList();
            return View();
        }

        public ActionResult _Fields(int moduleId)
        {
            var context = GetHrisContext();
            return View(context.ModuleScreenFields.Where(m => m.ModuleId == moduleId).OrderBy(m => m.ResourceName).ToList());
        }

        public JsonResult SaveScreenFields(List<ModuleScreenField> fields)
        {
            var context = GetHrisContext();

            if (fields != null)
            {
                foreach (ModuleScreenField child in fields)
                {
                    ModuleScreenField field = context.ModuleScreenFields.Where(f => f.Id == child.Id).FirstOrDefault();
                    field.Enabled = child.Enabled;
                }
            }

            context.SaveChanges();

            return Json(new { status = "success" });
        }

        #endregion

        #region Profile Permissions

        public ActionResult ProfilePermissions()
        {
            var context = GetKronosContext();

            ViewData["ProfileList"] = context.LOGONPROFILEs.Select(m => new HRISListObject { Description = m.PROFILEDSC, Value = m.LOGONPROFILEID.ToString() }).OrderBy(p=>p.Description).ToList();
            return View();
        }

        public ActionResult _Sections(int profileId)
        {
            var context = GetHrisContext();

            List<AppSection> secList = context.AppSections.OrderBy(s=>s.Name).ToList();
            List<ProfilePermission> grantedList = context.ProfilePermissions.Where(m => m.LogonProfileId == profileId).ToList();
            List<PermissionModel> list = new List<PermissionModel>();
            int notGrantedId = 0;
            foreach (AppSection child in secList)
            {
                PermissionModel model = new PermissionModel { Name = child.Name, ProfileId = profileId, SectionId = child.Id };               

                ProfilePermission granted = grantedList.Where(p => p.AppSectionId == child.Id && p.LogonProfileId == profileId).FirstOrDefault();
                if (granted != null)
                {
                    model.Id = granted.Id;
                    model.Checked = true;
                }
                else
                {
                    model.Id = --notGrantedId;
                    model.Checked = false;
                }

                list.Add(model);
            }

            return View(list);
        }

        public JsonResult SaveProfilePermissions(List<PermissionModel> permissions)
        {
            var context = GetHrisContext();

            if (permissions != null)
            {
                foreach (PermissionModel child in permissions)
                {
                    if (child.Id > 0 && child.Checked == false)
                    {
                        ProfilePermission perm = context.ProfilePermissions.Where(p => p.Id == child.Id).FirstOrDefault();
                        context.ProfilePermissions.Remove(perm);
                    }
                    else
                    {
                        if(child.Checked)
                        {
                            context.ProfilePermissions.Add(new ProfilePermission { AppSectionId = child.SectionId, LogonProfileId = child.ProfileId });
                        }
                    }
                }
            }

            context.SaveChanges();

            return Json(new { status = "success" });
        }

        #endregion
    }
}
