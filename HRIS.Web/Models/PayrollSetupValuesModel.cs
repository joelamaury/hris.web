﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class PayrollSetupValuesModel
    {
        public int PersonPayrollSetupValueSeqNo { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public DateTime FromEffectiveDate { get; set; }
        public DateTime ToEffectiveDate { get; set; }
     
    }
}