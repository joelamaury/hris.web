﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class PersonEmail
    {
        public int PersonIdNo { get; set; }
        public int EMailSeqNo { get; set; }
      
        public string EMailAddress { get; set; }
        public string Primary { get; set; }

        public System.DateTime EMailFromEffectDate { get; set; }
        public System.DateTime EMailToEffectDate { get; set; }
    }
}