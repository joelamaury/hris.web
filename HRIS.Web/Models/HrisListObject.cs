﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class HRISListObject
    {
        public HRISListObject() { }

        public HRISListObject(String Value, String Description)
        {
            this.Value = Value;
            this.Description = Description;
        }

        public String Value { get; set; }
        public String Description { get; set; }
    }
}