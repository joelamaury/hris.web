﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class PersonAddress
    {
        public int PersonIdNo { get; set; }
        public int PersonAddressSeqNo { get; set; }

        public string Address { get; set; }
        public string Type { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Primary { get; set; }

        public string PersonAddressPostalCode { get; set; }
        public System.DateTime PersonAddressFromEffectDate { get; set; }
        public System.DateTime PersonAddressToEffectDate { get; set; }
    }
}