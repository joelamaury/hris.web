﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class ProcessedFilesModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime Date { get; set; }
        public int ModuleCount { get; set; }
        public string Status { get; set; }
        public int ErrorWarningCount { get { return WarningCount + ErrorCount; } }
        public int SuccessCount { get; set; }
        public int WarningCount { get; set; }
        public int ErrorCount { get; set; }
    }

    public class ProcessedFileModulesModel
    {
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string Info   { get; set; }
        public string Status { get; set; }
        public string EmployeeId { get; set; }
        public string Log { get; set; }
    }
}