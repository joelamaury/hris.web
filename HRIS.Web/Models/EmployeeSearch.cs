﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class EmployeeSearch
    {
        public int PK1 { get; set; }
        public int PK2 { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeeId { get; set; }
        public string SSN { get; set; }
        public string PositionTitle { get; set; }
        public string SupervisorFullName { get; set; }
        public string Organizations { get; set; }
        public string OrganizationText { get; set; }
        public string StatusText { get; set; }
        public string Status { get; set; }
        public string EffectiveDate { get; set; }
        public string IsPrimaryPosition { get; set; }
        public bool Dupilcated { get; set; }

    }
}