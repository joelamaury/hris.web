﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class PersonPosition
    {
        public int PersonPositionSeqNo { get; set; }
        public int PersonIdNo { get; set; }

        public string PositionCode { get; set; }
        public string PositionDescription { get; set; }
        public string Primary { get; set; }
        public string JobCode { get; set; }


    }
}