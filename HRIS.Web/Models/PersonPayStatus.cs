﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class PersonPayStatus
    {
        public int PayStatusSeqNo { get; set; }
        public int PersonIdNo { get; set; }
        public int PositionIdNo { get; set; }

        public string EffectiveFrom { get; set; }
        public string CompensationType { get; set; }
        public string PayRate { get; set; }
        public string Frequency { get; set; }
        public string AnnualSalary { get; set; }
        public string Reason { get; set; }
        public string LastChangeDate { get; set; }
        public string NextChangeDate { get; set; }
        public string LastChangeAmount { get; set; }

    }
}