﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class PersonPhone
    {
        public int PersonIdNo { get; set; }
        public int PersonPhoneSeqNo { get; set; }

        public string Type { get; set; }
        public string Number { get; set; }
        public string Primary { get; set; }
        public string Extension { get; set; }

        public System.DateTime PersonPhoneFromEffectDate { get; set; }
        public System.DateTime PersonPhoneToEffectDate { get; set; }
    }
}