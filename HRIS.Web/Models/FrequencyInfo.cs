﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.Models
{
    public class FrequencyInfo
    {
        public int PayRateFrequencyCodeIdNo { get; set; }
        public string PayRateFrequencyCode { get; set; }
        public Nullable<int> AnnualizingFactor { get; set; }
    }
}