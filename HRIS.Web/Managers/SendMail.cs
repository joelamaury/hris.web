﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using NLog;

namespace HRIS.Managers
{
    public class SendMail
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void SendMailMessage(String strTo, String[] strBcc, String strSubject, String strBody)
        {
            try
            {
                String strServer = ConfigurationManager.AppSettings["Smtp.Server"].ToString();
                String strUsuario = ConfigurationManager.AppSettings["Smtp.Account"].ToString();
                String strClave = ConfigurationManager.AppSettings["Smtp.Password"].ToString();

                Int32 smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["Smtp.Port"]);
                Boolean ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["Smtp.UseSsl"]);
                Int32 timeout = int.MaxValue;
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Smtp.Timeout"]))
                    timeout = Convert.ToInt32(ConfigurationManager.AppSettings["Smtp.Timeout"]);

                String[] sendTo = strBcc;
                MailMessage oEmail = new MailMessage();
                oEmail.From = new System.Net.Mail.MailAddress(strUsuario);
                oEmail.To.Add(strTo);
                oEmail.Subject = strSubject;
                oEmail.Body = strBody;
                oEmail.IsBodyHtml = true;
                oEmail.Priority = System.Net.Mail.MailPriority.Normal;

                if (sendTo.Length > 0)
                {
                    for (int i = 0; i < sendTo.Length; i++)
                        if (!String.IsNullOrEmpty(sendTo[i]))
                            oEmail.CC.Add(sendTo[i]);
                }

                SmtpClient oCliente = new SmtpClient();
                oCliente.Host = strServer;
                oCliente.Port = smtpPort;

                if (!String.IsNullOrEmpty(strUsuario) && !String.IsNullOrEmpty(strClave))
                {
                    oCliente.Credentials = new System.Net.NetworkCredential(strUsuario, strClave);
                    oCliente.EnableSsl = ssl;                   
                    oCliente.Timeout = timeout;
                }

                oCliente.Send(oEmail);
            }
            catch (Exception ex)
            {
                logger.Fatal(ex);
                throw (ex);
            }
        }
    }
}