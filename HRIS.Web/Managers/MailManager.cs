﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NLog;

namespace HRIS.Managers
{
    public class MailManager 
    {
        #region Initialization
        //Singleton object creation
        private static MailManager instance;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        String emailsTemplatePath = ConfigurationManager.AppSettings["EmailsTemplates.Path"].ToString();
        String baseUrl = ConfigurationManager.AppSettings["BaseUrl"].ToString();

        private static object syncRoot = new Object();

        private MailManager() { }

        public static MailManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new MailManager();
                    }
                }
                return instance;
            }
        }
        #endregion

        #region Messages
        private String MessageRequestApprove(string FullNM, string Announcement, string DateApprove)
        {
            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "SolicitudAceptada.html")) select line;
            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[APPLICANTNAME]", FullNM).Replace("[CONVOCATORIA]", Announcement).Replace("[FechaAceptacion]", DateApprove));
            }
            return oBuilder.ToString();
        }

        private String MessageRequestRecived(string FullNM, string PositionName, string SubmittedDate)
        {
            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "SolicitudRecibida.html")) select line;

            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[APPLICANTNAME]", FullNM).Replace("[KronosPositionName]", PositionName).Replace("[SubmittedDate]", SubmittedDate));
            }
            return oBuilder.ToString();
        }

        private String MessageRequestRejected(string FullNM, string PositionName, string RejectReason)
        {
            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "SolicitudRechazada.html")) select line;

            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[APPLICANTNAME]", FullNM).Replace("[KronosPositionName]", PositionName).Replace("[RejectReason]", RejectReason));
            }
            return oBuilder.ToString();
        }

        private String MessageAppointmentReminder(string FullNM, string PositionName, string TestDate, string TestTime, string TestPlace)
        {
            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "Cita.html")) select line;

            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[APPLICANTNAME]", FullNM).Replace("[KronosPositionName]", PositionName).Replace("[TESTDATE]", TestDate).Replace("[TESTTIME]", TestTime).Replace("[TESTPLACE]", TestPlace));
            }
            return oBuilder.ToString();
        }

        private String MessageBodyResetPassword(String EmailTo, String NewPassword, String Name)
        {
            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "ResetPassword.html")) select line;

            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[PASSWORD]", NewPassword).Replace("[URL]", baseUrl + "/Login/Index/" ).Replace("[NAME]", Name));
            }
            return oBuilder.ToString();
        }

        private String MessageBodyForgotPassword(String EmailTo, String NewPassword, String Name)
        {
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "ForgotPassword.html")) select line;

            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[PASSWORD]", NewPassword).Replace("[URL]", baseUrl + "/Login/Index/").Replace("[NAME]", Name));
            }
            return oBuilder.ToString();
        }

        private String MessageBodyActivation(Int32 userId, String EmailTo, String Name)
        {
            // Levantar el template en memoria
            var templateFile = from line in File.ReadAllLines(Path.Combine(emailsTemplatePath, "ActivationTemplate.html")) select line;

            StringBuilder oBuilder = new StringBuilder();

            foreach (var item in templateFile)
            {
                oBuilder.Append(item.Replace("[NAME]", Name).Replace("[URL]", baseUrl + "/Login/RegisterConfirmation/" + userId));
            }
            return oBuilder.ToString();
        }
        #endregion

        #region SendMail
        public void SendMailMessageRequestApprove(string EmailTo, string EmailSubject,  string FullNM, string Announcement, DateTime DateApprove)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageRequestApprove(FullNM, Announcement, DateApprove.ToString("MM/dd/yyyy")));
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageRequestRecived(string EmailTo, string EmailSubject, string FullNM, string PositionName, DateTime SubmittedDate)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageRequestRecived(FullNM, PositionName, SubmittedDate.ToString("MM/dd/yyyy")));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageRequestRejected(string EmailTo, string EmailSubject, string FullNM, string PositionName, string RejectReason)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageRequestRejected(FullNM, PositionName, RejectReason));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SendMailMessageAppointmentReminder(string EmailTo, string EmailSubject, string FullNM, string PositionName, DateTime TestDate, string TestPlace)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageAppointmentReminder(FullNM, PositionName, TestDate.ToString("MM/dd/yyyy"), TestDate.ToString("hh:mm tt"),TestPlace));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        internal bool SendMailMessageUserActivation(String EmailSubject, Int32 UserId, String EmailTo, String Name)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageBodyActivation(UserId, EmailTo, Name));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }
            return true;
        }

        public Boolean SendMailMessageResetPassword(String EmailSubject, String EmailTo, String NewPassword, String Name)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" }, EmailSubject, MessageBodyResetPassword(EmailTo, NewPassword, Name));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }
            return true;
        }

        public Boolean SendMailMessageForgotPassword(String EmailSubject, String EmailTo, String NewPassword, String Name)
        {
            try
            {
                SendMail.SendMailMessage(EmailTo, new String[] { "" },  EmailSubject, MessageBodyResetPassword(EmailTo, NewPassword, Name));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }
            return true;
        }
        #endregion
    }
}