﻿var serviceURL = "";

$.validator.addMethod('regexpOneUpper', function (value, element, param) {
    return this.optional(element) || value.match(param);
},
    'This value doesn\'t match the acceptable pattern.');

$.validator.addMethod('regexpOneNumber', function (value, element, param) {
    return this.optional(element) || value.match(param);
},
    'This value doesn\'t match the acceptable pattern.');


function addError(error, element) {
    var parent = $(".loginForm").parent();
    var placement = $(element).data('error');
    if (placement) {
        $(placement).append(error)
    } else {
        error.appendTo($('.errorMsg'));

      /*  var contentHeight = parent.height();
        parent.height(contentHeight + error.height());*/
    }
}

$(document).ready(function () {
    $("#form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            user: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4,
                maxlength: 12
            },
            passwordLogin: {
                required: true
            },
            repassword: {
                equalTo: "#password"
            },
            lastName: {
                required: true
            },
            firstName: {
                required: true
            },
            chkPolicy: {
                required: true
            }
        },
       /* errorPlacement: function (error, element) {
            addError(error, element);
        },*/
        unhighlight: function (element, errorClass, validClass) {
            var errorLabel = $("#" + element.id + "-error");
            $("#" + element.id).removeClass("error");
            var contentHeight = $(".greyAutoHeight").height();
            $(".greyAutoHeight").height(contentHeight - errorLabel.height());
            errorLabel.remove();
        },
        submitHandler: function (form) 
        {            
            form.submit();
               // displayLoading("#loginFormFields");
               /* var formData = {
                    user: $("#user").val(),
                    password: $("#passwordLogin").val(),
                    rememberMe: $("#rememberMe").is(":checked"),
                    UserIp : UserIp //Value set on document ready on main.js
                };
                $.ajax({
                    type: "POST",
                    url: serviceURL + '/Login/Index',
                    data: JSON.stringify(formData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (results) 
                    {
                        $('.errorMsg').html("");
                        if (results.action == "error") 
                        {
                            hideLoading("#loginFormFields");
                            $('.errorMsg').html('<label class="error" id="errorMsg">' + results.msg + '</label>');
                            $('#passwordLogin').prop("value", "");
                        }
                        else if (results.action == "firstLogin")
                            window.location.href = serviceURL + "/Login/NewPassword";
                        else if (results.action == "success")
                            window.location.href = serviceURL + results.msg;
                    }
                });      */  
        },
        messages: {
            user: {
                required: LANG.user + LANG.errorRequired,
                email: LANG.errorMail,
            },
            username: {
                required: (LANG.username == 'Username') ? LANG.username + LANG.usernameRequired : LANG.usernameRequired + LANG.username,
                email: LANG.errorMail,
            },
            email: {
                required: LANG.email + LANG.errorRequired,
                email: LANG.errorMail
            },
            password: {
                required: LANG.password + LANG.errorRequired,
                password: LANG.errorInvalidPassword,
                minlength: LANG.passwordTooShort,
                maxlength: LANG.passwordTooLong
             
            },
            passwordLogin: {
                required: LANG.password + LANG.errorRequired
            },
            repassword: {
                equalTo: LANG.repassword + LANG.equalTo + LANG.password
            },
            lastName: {
                required: LANG.lastName + LANG.errorRequired,
            },
            firstName: {
                required: LANG.firstName + LANG.errorRequired,
            },
            chkPolicy: {
                required: LANG.YouMustAcceptTheTermsAndConditions,
            }, 
        }
    });

    /*$('#btnSetPassword').click(function () {
        $('#form').submit();
    });

    $('#buttonEmailMe').click(function () {
        $.ajax({
            type: 'POST', dataType: 'json', contentType: 'application/json, charset=utf-8',
            url: 'ValidateForgotPassword',
            data: JSON.stringify({
                username: $("#username").val()
            }),
            success: function (result) {
                if (result === "success") {
                    $('#form').submit();
                } else if (result == "UserNotAccount") {
                    $('.errorMsg').show();
                    $('.notexists').hide();
                    $('.notaccount').show();
                } else if (result == "UserNotExists") {
                    $('.notexists').show();
                    $('.notaccount').hide();
                    $('.errorMsg').show();
                }
            }
        });
    });

    $('#buttonEmailMeExt').click(function () {
        $.ajax({
            type: 'POST', dataType: 'json', contentType: 'application/json, charset=utf-8',
            url: 'ValidateForgotPasswordExt',
            data: JSON.stringify({
                username: $("#username").val()
            }),
            success: function (result) {
                if (result === "success") {
                    $('#form').submit();
                } else if (result == "UserNotAccount") {
                    $('.errorMsg').show();
                    $('.notexists').hide();
                    $('.notaccount').show();
                } else if (result == "UserNotExists") {
                    $('.notexists').show();
                    $('.notaccount').hide();
                    $('.errorMsg').show();
                } else {
                    alert(result);
                }
            }
        });
    });*/
});

$(document).ready(function ()
{
    $("#loginform").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            user: {
                required: true
            },

            password: {
                required: true              
            }
        },
        /* errorPlacement: function (error, element) {
             addError(error, element);
         },*/
        unhighlight: function (element, errorClass, validClass)
        {
            var errorLabel = $("#" + element.id + "-error");
            $("#" + element.id).removeClass("error");
            var contentHeight = $(".greyAutoHeight").height();
            $(".greyAutoHeight").height(contentHeight - errorLabel.height());
            errorLabel.remove();
        },
        submitHandler: function (form) 
        {
            form.submit();           
        },
        messages: {
            user: {
                required: LANG.user + LANG.errorRequired,
                email: LANG.errorMail,
            },
            username: {
                required: (LANG.username == 'Username') ? LANG.username + LANG.usernameRequired : LANG.usernameRequired + LANG.username,
                email: LANG.errorMail,
            },
            email: {
                required: LANG.email + LANG.errorRequired,
                email: LANG.errorMail
            },
            password: {
                required: LANG.password + LANG.errorRequired,
                password: LANG.errorInvalidPassword,
                minlength: LANG.passwordTooShort,
                maxlength: LANG.passwordTooLong
            },
            passwordLogin: {
                required: LANG.password + LANG.errorRequired
            },
            repassword: {
                equalTo: LANG.repassword + LANG.equalTo + LANG.password
            },
            lastName: {
                required: LANG.lastName + LANG.errorRequired,
            },
            firstName: {
                required: LANG.firstName + LANG.errorRequired,
            },
            chkPolicy: {
                required: LANG.YouMustAcceptTheTermsAndConditions,
            },
        }
    });
});

$(window).resize(function () {
    $('.centered').css({
        position: 'absolute',
        left: ($(window).width() - $('.centered').outerWidth()) / 2,
        top: ($(window).height() - $('.centered').outerHeight()) / 2 - 90
    });
});