﻿LANG = {
    user: "Usuario",
    password: "Contraseña",
    repassword: "Confirmación de contraseña",
    email: "Correo Electrónico",
    firstName: "Nombre",
    lastName: "Apellido",
    errorRequired: " es obligatorio.",
    errorMail: "Por favor ingrese un correo válido.",
    errorInvalidPassword: "Clave inválida.",
    equalTo: " debe ser igual a ",
    passwordTooShort: "La contraseña es demasiado corta (mínimo es de 4 caracteres).",
    passwordTooLong: "La contraseña es demasiado larga (máximo es de 4 caracteres).",
    mustContainAtLeastOneCharacter: "La contraseña debe contener una letra mayúscula.",
    mustContainAtLeastOneNumber: "La contraseña debe contener un número.",
    username: " usuario.",
    usernameRequired: "Debe ingresar",
    //Hyperfind
    StatusDescription:"Estado",
    emp_first_name: "Nombre",
    emp_last_name: "Apellido",
    emp_mid_initial:"Inicial",
    emp_id:"Id",
    emp_badge: "Tarjeta",
    Pr_Name: "Pay Rule",
    LaborAccount: "Centro de Costos",
    signoffdate: "Cierre de la Tarjeta",
    //
    YES: "SI",
    NO: "NO",
    Deduction: "Deducción",
    Hours: "Horas",
    Income: "Ingreso",
    Contribution: "Contribución",
    Category: "Categoría",
    PaymentTypeC: "Cheque",
    PaymentTypeD: "Depósito Directo",
    //
    month1: "Enero", 
    month2: "Febrero", 
    month3: "Marzo", 
    month4: "Abril", 
    month5: "Mayo", 
    month6: "Junio", 
    month7: "Julio", 
    month8: "Agosto",
    month9: "Septiembre",
    month10: "Octubre",
    month11: "Noviembre", 
    month12: "Deciembre",
    Select: "Seleccionar",
    YouMustAcceptTheTermsAndConditions: "Debes aceptar los términos y condiciones."
}