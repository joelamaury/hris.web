//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tPERSON_PHONES
    {
        public int PersonIdNo { get; set; }
        public int PersonPhoneSeqNo { get; set; }
        public int PersonPhoneTypeIdNo { get; set; }
        public bool PersonPhonePrimaryInd { get; set; }
        public string PersonPhoneNo { get; set; }
        public string PersonPhoneExt { get; set; }
        public System.DateTime PersonPhoneFromEffectDate { get; set; }
        public System.DateTime PersonPhoneToEffectDate { get; set; }
        public System.DateTime PersonPhoneChangeDate { get; set; }
        public byte[] Timestamp { get; set; }
    
        public virtual tTYPE_CODES tTYPE_CODES { get; set; }
        public virtual tPERSON_IDS tPERSON_IDS { get; set; }
    }
}
