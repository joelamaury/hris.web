//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tTYPE_CODES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tTYPE_CODES()
        {
            this.tORGANIZATION_TYPES = new HashSet<tORGANIZATION_TYPES>();
            this.tPAY_STATUS = new HashSet<tPAY_STATUS>();
            this.tPERSON_ADDRESSES = new HashSet<tPERSON_ADDRESSES>();
            this.tPERSON_PHONES = new HashSet<tPERSON_PHONES>();
            this.tPERSONS = new HashSet<tPERSON>();
            this.tPERSONS1 = new HashSet<tPERSON>();
            this.tTYPE_CODES1 = new HashSet<tTYPE_CODES>();
        }
    
        public int TypeCodeIdNo { get; set; }
        public int TypeCodeDomainIdNo { get; set; }
        public string TypeCode { get; set; }
        public string TypeCodeType { get; set; }
        public System.DateTime TypeCodeFromEffectDate { get; set; }
        public System.DateTime TypeCodeToEffectDate { get; set; }
        public string LanguageId { get; set; }
        public bool TypeCodeDefaultCodeInd { get; set; }
        public int WTKPhoneIdNo { get; set; }
        public byte[] Timestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tORGANIZATION_TYPES> tORGANIZATION_TYPES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPAY_STATUS> tPAY_STATUS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON_ADDRESSES> tPERSON_ADDRESSES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON_PHONES> tPERSON_PHONES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON> tPERSONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON> tPERSONS1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tTYPE_CODES> tTYPE_CODES1 { get; set; }
        public virtual tTYPE_CODES tTYPE_CODES2 { get; set; }
    }
}
