//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tJOB_CODE_IDS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tJOB_CODE_IDS()
        {
            this.tJOB_CODE_ORGS = new HashSet<tJOB_CODE_ORGS>();
            this.tJOB_CODES = new HashSet<tJOB_CODES>();
            this.tJOB_CODES1 = new HashSet<tJOB_CODES>();
            this.tPOSITION_CODES = new HashSet<tPOSITION_CODES>();
        }
    
        public int JobCodeIdNo { get; set; }
        public byte[] Timestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tJOB_CODE_ORGS> tJOB_CODE_ORGS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tJOB_CODES> tJOB_CODES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tJOB_CODES> tJOB_CODES1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPOSITION_CODES> tPOSITION_CODES { get; set; }
    }
}
