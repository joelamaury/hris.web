//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tPAY_GRADE_CODE_IDS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tPAY_GRADE_CODE_IDS()
        {
            this.tJOB_CODES = new HashSet<tJOB_CODES>();
            this.tPAY_GRADE_CODES = new HashSet<tPAY_GRADE_CODES>();
            this.tPAY_GRADE_CODES1 = new HashSet<tPAY_GRADE_CODES>();
            this.tPAY_STATUS = new HashSet<tPAY_STATUS>();
        }
    
        public int PayGradeIdNo { get; set; }
        public byte[] Timestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tJOB_CODES> tJOB_CODES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPAY_GRADE_CODES> tPAY_GRADE_CODES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPAY_GRADE_CODES> tPAY_GRADE_CODES1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPAY_STATUS> tPAY_STATUS { get; set; }
    }
}
