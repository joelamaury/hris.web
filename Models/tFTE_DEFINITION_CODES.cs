//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tFTE_DEFINITION_CODES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tFTE_DEFINITION_CODES()
        {
            this.tFTE_DEFINITION_CODE_VALUES = new HashSet<tFTE_DEFINITION_CODE_VALUES>();
        }
    
        public int FTEDefinitionIdNo { get; set; }
        public int FTEDefinitionSeqNo { get; set; }
        public string FTECode { get; set; }
        public string FTEDescription { get; set; }
        public Nullable<decimal> FTEHoursPerWeek { get; set; }
        public Nullable<bool> InactiveInd { get; set; }
        public System.DateTime FTEFromEffectDate { get; set; }
        public System.DateTime FTEToEffectDate { get; set; }
        public byte[] FTECodeTimestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tFTE_DEFINITION_CODE_VALUES> tFTE_DEFINITION_CODE_VALUES { get; set; }
        public virtual tFTE_DEFINITION_IDS tFTE_DEFINITION_IDS { get; set; }
    }
}
