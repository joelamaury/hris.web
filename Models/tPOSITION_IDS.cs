//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tPOSITION_IDS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tPOSITION_IDS()
        {
            this.tORGANIZATIONS = new HashSet<tORGANIZATION>();
            this.tPAY_STATUS = new HashSet<tPAY_STATUS>();
            this.tPERSON_POSITIONS = new HashSet<tPERSON_POSITIONS>();
            this.tPOSITION_CODES = new HashSet<tPOSITION_CODES>();
            this.tPOSITION_CODES1 = new HashSet<tPOSITION_CODES>();
            this.tPOSITION_CODES2 = new HashSet<tPOSITION_CODES>();
            this.tPOSITION_ORGS = new HashSet<tPOSITION_ORGS>();
        }
    
        public int PositionIdNo { get; set; }
        public byte[] Timestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tORGANIZATION> tORGANIZATIONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPAY_STATUS> tPAY_STATUS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON_POSITIONS> tPERSON_POSITIONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPOSITION_CODES> tPOSITION_CODES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPOSITION_CODES> tPOSITION_CODES1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPOSITION_CODES> tPOSITION_CODES2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPOSITION_ORGS> tPOSITION_ORGS { get; set; }
    }
}
