//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tWCMP_WORKERS_COMPENSATION_CODES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tWCMP_WORKERS_COMPENSATION_CODES()
        {
            this.tPERSON_POSITIONS = new HashSet<tPERSON_POSITIONS>();
        }
    
        public int WorkersCompensationIdNo { get; set; }
        public int LocalCodeIdNo { get; set; }
        public string WorkersCompensationCode { get; set; }
        public string WorkersCompensationCodeDescription { get; set; }
        public decimal WorkerCompRate { get; set; }
        public System.DateTime WorkerCompFromEffectDate { get; set; }
        public System.DateTime WorkerCompToEffectDate { get; set; }
        public byte[] Timestamp { get; set; }
    
        public virtual tLOCAL_CODES tLOCAL_CODES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON_POSITIONS> tPERSON_POSITIONS { get; set; }
    }
}
