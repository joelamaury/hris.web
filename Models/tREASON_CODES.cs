//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tREASON_CODES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tREASON_CODES()
        {
            this.tEMPLOYMENT_STATUS = new HashSet<tEMPLOYMENT_STATUS>();
            this.tPAY_STATUS = new HashSet<tPAY_STATUS>();
            this.tPERSON_POSITIONS = new HashSet<tPERSON_POSITIONS>();
            this.tREASON_CODES1 = new HashSet<tREASON_CODES>();
        }
    
        public int ReasonCodeIdNo { get; set; }
        public int ReasonCodeDomainIdNo { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonCodeType { get; set; }
        public System.DateTime ReasonCodeFromEffectDate { get; set; }
        public System.DateTime ReasonCodeToEffectDate { get; set; }
        public string LanguageId { get; set; }
        public bool ReasonCodeDefaultCodeInd { get; set; }
        public byte[] Timestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tEMPLOYMENT_STATUS> tEMPLOYMENT_STATUS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPAY_STATUS> tPAY_STATUS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPERSON_POSITIONS> tPERSON_POSITIONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tREASON_CODES> tREASON_CODES1 { get; set; }
        public virtual tREASON_CODES tREASON_CODES2 { get; set; }
    }
}
